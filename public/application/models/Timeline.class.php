<?php

/**
 * Description of Timeline
 *
 * @author simon
 * 
 * important note: 
 * job contains StatusHistoryOldID for the status for the current status
 * status_history_old conatins all the status for each job
 */
require_once('DataObject.class.php');
class Timeline extends DataObject {
    private $stage, $current_stage, $current_date, $exceed = false;
    
    /***************************************************************************
     * switch aback to the old single status timeline
     * 
     * pls also change line 35 to 36 as order of each timeline maybe different
     * 
     * 
     ***************************************************************************/
    private $status = 'status_old',
            $status_history = 'status_history_old';

    private static  $timeline;
    
    /*
     * @param PDO $conn
     * @param string $jobID
     * @param string $current_status
     * 
     * @todo
     * remove $current_status and get it from the db using JobID also get JobTurnaroundTime
     * or replace JobID with all the require info from Job->fetch
     * 
     */
    public function __construct($conn, $jobID, $current_status) {
        parent::__construct($conn);

        $data = $this->collect($this->status, null, '*', '`Sort` ASC');
        #$data = $this->collect('status_old', 'Branch != 0', '*', '`Branch` ASC');
        
        #new timeline will use status_history table
        $stages = $this->collect($this->status_history.' AS t1 LEFT JOIN '.$this->status.' AS t2 ON t1.StatusID = t2.StatusID', array('JobID' => $jobID), '*', 'Date DESC');
        
        #var_dump( $stages );
        $tense = 'past';
           
        $DateFormat = DataObject::$default['DateFormat'];

        
        $timeline = array();
        foreach($data as $d){

            $status = $this->collectByID('status_history_old AS t1 LEFT JOIN status_old AS t2 ON t1.StatusID = t2.StatusID',
                    array('JobID' => $jobID, 't2.StatusID' => $d['StatusID']),
                    "t1.*, DATE_FORMAT(Date, '{$DateFormat}') AS Date",
                    'Date DESC');   
            
            #var_dump( $status );
                    #echo $current_status .'-' .$status['StatusHistoryID'];
            
            $stage = array();
            if($current_status == $status['StatusHistoryID']){
                $stage['Tense'] = 'present';
                
                $this->current_date = $status['Date'];
                $tense = 'future';
                $test = new Stage('future');
            }else{
                $stage['Tense'] = $tense;
                $test = new Stage();
            }
            #echo $test::$Tense;
            #echo $test->Name;
            
            $stage['Name'] = $d['StatusName'];
            $stage['Date'] = (isset($status['Date']) ? $status['Date'] : '');
            $stage['By'] = (isset($status['By']) ? $status['By'] : '');
            
            $timeline[$d['StatusID']] = $stage;  
        }# end foreach
            
        #var_dump($timeline);
        self::$timeline = $timeline;
        
    }
    
    public function getDaysFromBooking(){
        $bookedDate = $this->formatDate(self::$timeline['b']['Date']);
        $currentDate = $this->formatDate($this->current_date);
        
        return $this->dateDiff($bookedDate, $currentDate);
    }
    
    public function getRemainingDays(){
        
        return 10 - $this->getDaysFromBooking();
    } 

    public function getRemainingExceededDays(){
        $value = $this->getDaysFromBooking();
        $gauge = DataObject::$default['StandardJobTurnaroundTime'];
        if($value < $gauge){
            $remaining = $gauge - $value;
            return ($remaining < 2 ? "$remaining Day Remaining" : "$remaining Days Remaining");
        } else {
            $exceed = $value - $gauge;
            return ($exceed < 2 ? "$exceed Day Exceeded" : "$exceed Days Exceeded");
        }
    }      
    
    public function formatDate($date){

        list($d, $m, $y) = explode('/', $date);
        return "$y-$m-$d";
    }
    
    /**
     * Finds the difference in days between two calendar dates.
     *
     * @param Date $startDate
     * @param Date $endDate
     * @return Int
     */
    function dateDiff($startDate, $endDate) {
        // Parse dates for conversion
        $startArry = date_parse($startDate);
        $endArry = date_parse($endDate);

        // Convert dates to Julian Days
        $start_date = gregoriantojd($startArry["month"], $startArry["day"], $startArry["year"]);
        $end_date = gregoriantojd($endArry["month"], $endArry["day"], $endArry["year"]);

        // Return difference
        return round(($end_date - $start_date), 0);
    }    
    
    public function getTimeline(){
        return self::$timeline;
    }
    
    //need to get TurnaroundTimeType value from job table
    public function TurnaroundTimeType(){
        $value = $this->getDaysFromBooking();
        $gauge = DataObject::$default['StandardJobTurnaroundTime'];
        if($value < $gauge)
            return '';
        else
            return '-red';       
    }
    
    public function getTurnaroundTimeValue(){
        $value = $this->getDaysFromBooking();
        $gauge = DataObject::$default['StandardJobTurnaroundTime'];        
        if($value < $gauge)
            return round(($value/$gauge*902), 0);
        else
            return round(($gauge/$value*902), 0);
    }
    
}


/*
 * This class is intended to setup template for each stage of the timeline
 */
class Stage {
    private $Name ='new ';
    
    public static $Tense = 'present';
    
    function __construct($tense =  null) {
        if($tense)
            $this::$Tense = $tense;
    }
    
    function __get($name) {
        if(isset($this->$name))
            return $this->$name;
    }
}
    
?>
