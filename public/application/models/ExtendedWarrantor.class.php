<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Extended Warrantor Page in Organisation Setup section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */

class ExtendedWarrantor extends CustomModel {
    
    private $conn;
    private $dbColumns = array('ExtendedWarrantorID', 'ExtendedWarrantorName', 'Status');
    private $table     = "extended_warrantor";
    
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->tables
     * @global $this->dbColumns
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
    public function fetch($args) {
        
        
      
           $output = $this->ServeDataTables($this->conn, $this->table, $this->dbColumns, $args);
        
        
            return  $output;
        
     }
    
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
    
     public function processData($args) {
         
         if(!isset($args['ExtendedWarrantorID']) || !$args['ExtendedWarrantorID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
     
     
    
    
    
    /**
     * Description
     * 
     * This method is used for to validate status name.
     *
     * @param interger $ExtendedWarrantorName  
     * @param interger $ExtendedWarrantorID.
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValidAction($ExtendedWarrantorName, $ExtendedWarrantorID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT ExtendedWarrantorID FROM '.$this->table.' WHERE ExtendedWarrantorName=:ExtendedWarrantorName AND ExtendedWarrantorID!=:ExtendedWarrantorID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':ExtendedWarrantorName' => $ExtendedWarrantorName, ':ExtendedWarrantorID' => $ExtendedWarrantorID));
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && $result['ExtendedWarrantorID'])
        {
                return false;
        }
        
        return true;
    
    }
    
    
    /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args  
     * @global $this->table 
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function create($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'INSERT INTO '.$this->table.' (ExtendedWarrantorName, Status, CreatedDate, ModifiedUserID, ModifiedDate)
            VALUES(:ExtendedWarrantorName, :Status, :CreatedDate, :ModifiedUserID, :ModifiedDate)';
        
        
        if($this->isValidAction($args['ExtendedWarrantorName'], 0))
        {
            $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
          
            
            $insertQuery->execute(array(':ExtendedWarrantorName' => $args['ExtendedWarrantorName'], ':Status' => $args['Status'], 
                
                ':CreatedDate' => date("Y-m-d H:i:s"),
                ':ModifiedUserID' => $this->controller->user->UserID,
                ':ModifiedDate' => date("Y-m-d H:i:s")
                
                ));
        
        
              return array('status' => 'OK',
                        'message' => $this->controller->page['Text']['data_inserted_msg']);
        }
        else
        {
            
            return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
     /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @global $this->table  
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT ExtendedWarrantorID, ExtendedWarrantorName, Status FROM '.$this->table.' WHERE ExtendedWarrantorID=:ExtendedWarrantorID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':ExtendedWarrantorID' => $args['ExtendedWarrantorID']));
        $result = $fetchQuery->fetch();
        
        return $result;
    }
    
    
      /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
     * @global $this->table   
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function update($args) {
        
        if($this->isValidAction($args['ExtendedWarrantorName'], $args['ExtendedWarrantorID']))
        {        
            
            $EndDate = "0000-00-00 00:00:00";
            $row_data = $this->fetchRow($args);
            if($this->controller->statuses[1]['Code']==$args['Status'])
            {
                if($row_data['Status']!=$args['Status'])
                {
                        $EndDate = date("Y-m-d H:i:s");
                }
            }
            
               /* Execute a prepared statement by passing an array of values */
              $sql = 'UPDATE '.$this->table.' SET 
                
              ExtendedWarrantorName=:ExtendedWarrantorName, Status=:Status, EndDate=:EndDate, ModifiedUserID=:ModifiedUserID, ModifiedDate=:ModifiedDate

              WHERE ExtendedWarrantorID=:ExtendedWarrantorID';
        
              $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
              $updateQuery->execute(
                      
                      array(
                        
                        ':ExtendedWarrantorName' => $args['ExtendedWarrantorName'], 
                        ':Status' => $args['Status'],
                        ':EndDate' => $EndDate,
                        ':ModifiedUserID' => $this->controller->user->UserID,
                        ':ModifiedDate' => date("Y-m-d H:i:s"),
                        ':ExtendedWarrantorID' => $args['ExtendedWarrantorID']
                
                )
                      
             );
        
                
               return array('status' => 'OK',
                        'message' => $this->controller->page['Text']['data_updated_msg']);
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => $this->controller->page['Text']['data_deleted_msg']);
    }
    
    
    
}
?>