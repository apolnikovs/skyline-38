<script>

    $(document).ready(function() {
    
    });

</script>

<fieldset id="rapidProcessModal" style="display:block; position:relative; padding-right:20px;">
    
    <legend align="center">Rapid Despatch Process</legend>
    
    <div id="leftDiv" style="width:60%; position:relative; float:left;">
	
	<span style="margin-top:10px; display:block;">
	    <label class="label" for="despatchDate">Despatch Date:</label>
	    <input type="text" value="" name="despatchDate" id="despatchDate" tabindex="1" />
	</span>
	
	<label class="label" for="consignmentNo">Consignment No:</label>
	<input type="text" value="" name="consignmentNo" id="consignmentNo" tabindex="2" />
	
	<span style="margin:5px 0px;; display:block;">
	    <label class="label">Despatch Type:</label>
	    <input type="radio" value="bulk" name="despatchType" />
	    <label for="despatchType">Bulk</label>
	    <input type="radio" value="individual" name="despatchType" />
	    <label for="despatchType">Individual</label>
	    <input type="radio" value="selected" name="despatchType" />
	    <label for="despatchType">Selected</label>
	</span>
	
	<label class="label" for="jobNo">Job No:</label>
	<input type="text" value="" name="jobNo" id="jobNo" tabindex="3" />
	
	<br/>
	
	<span id="nextWrap" style="display:block; width:100%; text-align:center; margin-bottom:30px; margin-top:5px;">
	    <a tabindex="4" href="#" id="next" class="btnStandard" style="width:50px;">Next</a>
	</span>
	
	<br/>  
	
    </div>
    
    <span style="display:block; position:absolute; bottom:10px; left:8px; width:60%; text-align:center;">
	<a href="#" id="finish" class="btnConfirm" style="width:50px; margin-right:10px;">Finish</a>
	<a href="#" id="printRapid" class="btnConfirm" style="width:80px; margin-right:10px;">Finish & Print</a>
	<a href="#" id="cancel" class="btnCancel" style="width:50px;">Cancel</a>
    </span>
	
    <div id="rightDiv" style="width:40%; display:block; position:relative; float:left;">
	
	<p style="text-align:center; margin-bottom:10px; margin-top:12px;">Updated Jobs</p>
	
	<table id="rapidTable" class="browse dataTable" style="margin-bottom:15px;">
	    <thead>
		<tr>
		    <th>Job No</th>
		    <th>Consignment No</th>
		    <th style="width:15%;"></th>
		</tr>
	    </thead>
	    <tbody>
	    </tbody>
	</table>
	
    </div>
    
    <span style="display:block; position:absolute; bottom:10px; right:0; width:40%; text-align:center;">
	<span>Total to despatch:</span>
	<span id="total">0</span>
    </span>
    
</fieldset>