{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $Manufacturers}
{/block}


{block name=scripts}


    {*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}
    <script type="text/javascript" src="{$_subdomain}/js/ajaxfileupload.js"></script>

    <script type="text/javascript">
    
     var $statuses = [
                    {foreach from=$statuses item=st}
                       ["{$st.Name}", "{$st.Code}"],
                    {/foreach}
                    ]; 
     
                 
    
                    
        
    function gotoEditPage($sRow)
    {
        
         
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
       
       
    }

    /**
    *  This is used to change color of the row if its in-active.
    */
    function inactiveRow(nRow, aData)
    {
          
        if (aData[3]==$statuses[1][1])
        {  
            $(nRow).addClass("inactive");

            $('td:eq(2)', nRow).html( $statuses[1][0] );
        }
        else
        {
            $(nRow).addClass("");
            $('td:eq(2)', nRow).html( $statuses[0][0] );
        }
        
        
         //Getting assigned value for each Manufacturer for selected Network.
         
        if(aData[4])
        {
           
            
            
           $.post("{$_subdomain}/ProductSetup/manufacturers/getAssigned/"+urlencode(aData[4])+"/"+urlencode($("#nId").val())+"/"+urlencode($("#cId").val()),        

            '',      
            function(data){
                    var p = eval("(" + data + ")");
                    
                    if(p)
                    {   
                        $checked = ' checked="checked" ';
                    }
                    else
                    {
                        $checked = '  ';
                    }
                    $checkbox = '<input type="checkbox" class="checkBoxDataTable" name="assignedManufacturers[]" '+$checked+' value="'+aData[4]+'" > ';
                    $('td:eq(3)', nRow).html( $checkbox );

            });
        }
        
    }
    
    
    
    function ajaxFileUpload()
    {
           
            
            $.ajaxFileUpload
            (
                    {
                            url:'{$_subdomain}/ProductSetup/manufacturers/uploadImage/'+urlencode($("#UploadedManufacturerLogo").val()),
                            secureuri:false,
                            fileElementId:'ManufacturerLogo',
                            dataType: 'json',
                            data:{ name:'logan', id:'id' }
                           
                    }
            )

            return true;

    }
    
   
 

    $(document).ready(function() {




                  //Click handler for finish button.
                    $(document).on('click', '#finish_btn', 
                                function() {

                                
                                     $(location).attr('href', '{$_subdomain}/SystemAdmin/index/productSetup');

                                });

                    //Click handler for save changes button.
                    $(document).on('click', '#save_changes_btn', 
                                function() {
                                    
                                    sltMnfr = '';
                                    
                                    $('.checkBoxDataTable').each(function() {
                                       sltMnfr += $(this).val()+',';
                                    });
                                       
                                    $("#sltMnfr").val(sltMnfr);  
                                  
                                  
                                    $.post("{$_subdomain}/ProductSetup/manufacturers/saveChanges/"+urlencode($("#nId").val())+"/"+urlencode($("#cId").val()),        

                                        $("#ManufacturersResultsForm").serialize(),      
                                        function(data){
                                        
                                               var p = eval("(" + data + ")");

                                               if(p=="OK")
                                               {
                                                   $("#centerInfoText").html("{$page['Text']['save_changes_done']|escape:'html'}").fadeIn('slow').delay("2000").fadeOut('slow');  
                                               }
                                               else 
                                               {
                                                   $("#centerInfoText").html("{$page['Text']['save_changes_error']|escape:'html'}").css('color','red').fadeIn('slow').delay("2000").fadeOut('slow');  
                                               }
                                               
                                                     
                                            
                                        });



                                
                                    // $(location).attr('href', '{$_subdomain}/SystemAdmin/index/productSetup');

                                });                    




                                


                      

                     /* Add a change handler to the network dropdown - strats here*/
                        $(document).on('change', '#nId', 
                            function() {
                                
                                $showAssigned = '';
                                if($("#showAssigned").prop("checked"))
                                    {
                                        $showAssigned = 1;    
                                    }
                               
                            
                                $(location).attr('href', '{$_subdomain}/ProductSetup/manufacturers/'+urlencode($("#nId").val())+'/'+$showAssigned); 
                            }      
                        );
                      /* Add a change handler to the network dropdown - ends here*/
                      
                    /* Add a click handler to the show assigned only check box - strats here*/
                        $(document).on('click', '#showAssigned', 
                            function() {
                                
                                 $showAssigned = '';
                                if($("#showAssigned").prop("checked"))
                                    {
                                        $showAssigned = 1;    
                                    }
                            
                            
                                $(location).attr('href', '{$_subdomain}/ProductSetup/manufacturers/'+urlencode($("#nId").val())+'/'+$showAssigned + '/' + urlencode($("#cId").val())); 
                            }      
                        );
                      /* Add a click handler to the show assigned only check box - ends here*/ 
                     
                     
                     
                     //Add a change handler to the client dropdown - starts here
                    $(document).on('change', '#cId', function() {
                        $showAssigned = '';
                        if($("#showAssigned").prop("checked")) {
                            $showAssigned = 1;    
                        }
                        $(location).attr('href', '{$_subdomain}/ProductSetup/manufacturers/' + urlencode($("#nId").val()) + "/" + $showAssigned + '/' + urlencode($("#cId").val())); 
                    });
                    //Add a change handler to the client dropdown - ends here
                     
                     
              

                    /* =======================================================
                    *
                    * set tab on return for input elements with form submit on auto-submit class...
                    *
                    * ======================================================= */

                    $('input[type=text],input[type=password]').keypress( function( e ) {
                            if (e.which == 13) {
                                $(this).blur();
                                if ($(this).hasClass('auto-submit')) {
                                    $('.auto-hint').each(function() {
                                        $this = $(this);
                                        if ($this.val() == $this.attr('title')) {
                                            $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                            if ($this.hasClass('auto-pwd')) {
                                                $this.prop('type','password');
                                            }
                                        }
                                    } );
                                    $(this).get(0).form.onsubmit();
                                } else {
                                    $next = $(this).attr('tabIndex') + 1;
                                    $('[tabIndex="'+$next+'"]').focus();
                                }
                                return false;
                            }
                        } );        



                     {if $SupderAdmin eq true} 

                        var  displayButtons = "UA";
                     
                     {else}

                         var displayButtons =  "U";

                     {/if} 
                         
                         
                         
                        if($("#nId").val() || $("#cId").val())
                        {
                            $("#showAssignedPanel").fadeIn();
                            $columnsList    = [ 
                                                        /* ManufacturerID */  {  "bVisible":    false },    
                                                        /* ManufacturerName */   null,
                                                        /* Acronym */   null,
                                                        /* Status */  null,
                                                        /* Assigned */   {  "bSearchable":    false }
                                                ];
                            $('#save_changes_btn').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
                         
                        }
                        else
                        {
                            $("#showAssignedPanel").fadeOut();
                            $columnsList    = [ 
                                                        /* ManufacturerID */  {  "bVisible":    false },    
                                                        /* ManufacturerName */   null,
                                                        /* Acronym */   null,
                                                        /* Status */  {  "bSearchable":    false }
                                                ];
                             $('#save_changes_btn').attr('disabled','disabled').removeClass('gplus-blue').addClass('gplus-blue-disabled');                    
                        }
     
                   
                    
                    $('#ManufacturersResults').PCCSDataTable( {
			aoColumns:	    $columnsList,
			aaSorting:	    [[ 1, "asc" ]],
			displayButtons:	    displayButtons,
			addButtonId:	    'addButtonId',
			addButtonText:	    '{$page['Buttons']['insert']|escape:'html'}',
			createFormTitle:    '{$page['Text']['insert_page_legend']|escape:'html'}',
			createAppUrl:	    '{$_subdomain}/ProductSetup/manufacturers/insert/',
			createDataUrl:	    '{$_subdomain}/ProductSetup/ProcessData/Manufacturers/',
			createFormFocusElementId: 'ManufacturerName',
			formInsertButton:   'insert_save_btn',
                            
			frmErrorRules: {	
			    ManufacturerName:		{ required: true },
                            Acronym:		{ required: true },    
			    AuthorisationRequired:	{ required: true },
			    AuthorisationManager:	{ required: function() { 
									var val = $("input[name=AuthorisationRequired]:checked").val();
									return (val == "Yes") ? true : false;
								    } 
							},
			    AuthorisationManagerEmail:	{ required: function() { 
									var val = $("input[name=AuthorisationRequired]:checked").val();
									return (val == "Yes") ? true : false;
								    } 
							},
			},

			frmErrorMessages: {
			    ManufacturerName:	{ required: "{$page['Errors']['name']|escape:'html'}" },
                            Acronym:		{ required: "{$page['Errors']['acronym']|escape:'html'}" },    
			    AuthorisationRequired:	{ required: "{$page['Errors']['auth_required']|escape:'html'}" },
			    AuthorisationManager:	{ required: "{$page['Errors']['auth_manager']|escape:'html'}" },
			    AuthorisationManagerEmail:	{ required: "{$page['Errors']['auth_manager_email']|escape:'html'}" }
			},                     
                            
			popUpFormWidth:	    750,
			popUpFormHeight:    430,
			updateButtonId:	    'updateButtonId',
			updateButtonText:   '{$page['Buttons']['edit']|escape:'html'}',
			updateFormTitle:    '{$page['Text']['update_page_legend']|escape:'html'}',
			updateAppUrl:	    '{$_subdomain}/ProductSetup/manufacturers/update/' + urlencode("{$nId}") + '/',
			updateDataUrl:	    '{$_subdomain}/ProductSetup/ProcessData/Manufacturers/',
			formUpdateButton:   'update_save_btn',
			updateFormFocusElementId:'ManufacturerName',
			colorboxFormId:	    "ManufacturersForm",
			frmErrorMsgClass:   "fieldError",
			frmErrorElement:    "label",
			htmlTablePageId:    'ManufacturersResultsPanel',
			htmlTableId:	    'ManufacturersResults',
			fetchDataUrl:	    '{$_subdomain}/ProductSetup/ProcessData/Manufacturers/fetch/'+urlencode("{$nId}")+"/"+urlencode("{$showAssigned}")+"/"+urlencode("{$cId}"),
			formCancelButton:   'cancel_btn',
			dblclickCallbackMethod: 'gotoEditPage',
			fnRowCallback:	    'inactiveRow',
			searchCloseImage:   '{$_subdomain}/css/Skins/{$_theme}/images/close.png',
			tooltipTitle:	    "{$page['Text']['tooltip_title']|escape:'html'}",
			iDisplayLength:	    25,
			formDataErrorMsgId: "suggestText",
			frmErrorSugMsgClass:"formCommonError",
			sDom:		    'ft<"#dataTables_command">rpli',
			bottomButtonsDivId: 'dataTables_command',
			insertBeforeSendMethod: "ajaxFileUpload",
			updateBeforeSendMethod: "ajaxFileUpload",
                        onSuccess: function( mode, data ) { 
                                           {* update AuthMan on success only if INSERT mode... *}
                                           //console.log('mode = '+mode);
                                           if (mode == 1) PostAuthMan( data['id'] ); 
                                       }
		    });
		    
                      

                        
                   

    });


    </script>

    
{/block}


{block name=body}

    <div class="breadcrumb">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/productSetup" >{$page['Text']['product_setup']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}

        </div>
    </div>



    <div class="main" id="home" >

               <div class="ServiceAdminTopPanel" >
                    <form id="ManufacturersTopForm" name="ManufacturersTopForm" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >{$page['Text']['legend']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['description']|escape:'html'}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  


                <div class="ServiceAdminResultsPanel" id="ManufacturersResultsPanel" >
                    
                    {if $SupderAdmin eq true} 
                        <form id="nIdForm" class="nidCorrections">
                        <select name="nId" id="nId" >
                            <option value="" {if $nId eq ''}selected="selected"{/if}>{$page['Text']['select_service_network']|escape:'html'}</option>

                            {foreach $networks as $network}

                                <option value="{$network.NetworkID}" {if $nId eq $network.NetworkID}selected="selected"{/if}>{$network.CompanyName|escape:'html'}</option>

                            {/foreach}
                        </select>
                        
                        
                        &nbsp;&nbsp;                        
                        <select name="cId" id="cId" >
                            <option value="" {if $cId eq ''}selected="selected"{/if}>{$page['Text']['select_client']|escape:'html'}</option>

                            {foreach $clients as $client}

                                <option value="{$client.ClientID}" {if $cId eq $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>

                            {/foreach}
                        </select>
                        
                        
                        
                        
                        <span id="showAssignedPanel" style="display:none;" >
                        <input class="assignedTextBox" id="showAssigned"  type="checkbox" name="showAssigned"  value="1" {if $showAssigned neq ''} checked="checked" {/if}  /><span class="text" >{$page['Text']['show_assigned']|escape:'html'}</span> 
                        </span>
                        
                        </form>
                    {else}
                        
                        <input type="hidden" name="nId" id="nId" value="{$nId|escape:'html'}" >
                        <input type="hidden" name="cId" id="cId" value="{$cId|escape:'html'}" >
                        
                    {/if} 
                    <br><br>
                    
                    <form id="ManufacturersResultsForm" class="dataTableCorrections">
                        <table id="ManufacturersResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                            <thead>
                                    <tr>
                                            <th></th> 
                                            <th title="{$page['Text']['manufacturer']|escape:'html'}" >{$page['Text']['manufacturer']|escape:'html'}</th>
                                            <th title="{$page['Text']['acronym']|escape:'html'}"  >{$page['Text']['acronym']|escape:'html'}</th>
                                            <th title="{$page['Text']['status']|escape:'html'}"  >{$page['Text']['status']|escape:'html'}</th>
                                            {if $nId neq ''}
                                                <th title="{$page['Text']['assigned']|escape:'html'}" >{$page['Text']['assigned']|escape:'html'}</th>
                                            {/if}
                                    </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>  
                        <input type="hidden" name="sltMnfr" id="sltMnfr" >
                     </form>
                </div>        

                <div class="bottomButtonsPanel" >
                    <hr>
                    <button id="save_changes_btn" type="button" class="gplus-blue-disabled leftBtn" ><span class="label">{$page['Buttons']['save_changes']|escape:'html'}</span></button>
                   
                    
                    <button id="finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
                    
                </div>        
                 <div class="centerInfoText" id="centerInfoText" style="display:none;" ></div>
               

                {if $SupderAdmin eq false} 

                   <input type="hidden" name="addButtonId" id="addButtonId" > 

                {/if} 
               


    </div>
                        
                        



{/block}



