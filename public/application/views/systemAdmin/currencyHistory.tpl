
<script type="text/javascript">   
     
 $(document).ready(function() {
 
 
    $('#CurrencyHistoryResults').dataTable(

{
"sDom": '<"left"><"top">t<"bottom"><"centered"><"right">pli<"clear">',

//"sDom": 'Rlfrtip',


                        
"bPaginate": true,
"sPaginationType": "full_numbers",
"aLengthMenu": [[ 25, 50, 100 , -1], [25, 50, 100, "All"]],
"iDisplayLength" : 25
 
   
 
        
          
}
);

 

   
   });
  </script>  



  <div>
        
        <fieldset>
                        <legend title="" >Currency History</legend>
                        <p>
                            <table id="CurrencyHistoryResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                                <thead>
                                    <tr>
                                       
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>From</th>
                                        <th>To</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {foreach $data as $d}
                                    <tr>
                                        <td>
                                        {$d.ChangeTimestamp|date_format:"%d/%m/%Y"}
                                        </td>
                                        <td>
                                        {$d.ChangeTimestamp|date_format:"%H:%M"}
                                        </td>
                                        <td>
                                        {$d.RateFrom}
                                        </td>
                                        <td>
                                        {$d.RateTo}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                            </table>
                            
                        </p> 

                        
                      
                        </fieldset> 
                                <br>
                                <button type="button" onclick="$.colorbox.close();" style="float:right" class="gplus-blue">Close</button>
  </div>    
                        
       

                 
 
                          
                        
