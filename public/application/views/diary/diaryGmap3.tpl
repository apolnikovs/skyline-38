<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
     <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/jquery-ui-1.9.1.custom.min.css" type="text/css" charset="utf-8" /> 
    
<!--    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>-->
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>-->
<!--<script src="http://code.jquery.com/ui/1.9.1/jquery-ui.js"></script>-->
<!--<script type="text/javascript" src="{$_subdomain}/js/ui/jquery.ui.map.full.min.js"></script>-->
<!--<script type="text/javascript" src="{$_subdomain}/js/ui/jquery.ui.map.extensions.js"></script>-->
<!--<script type="text/javascript" src="{$_subdomain}/js/ui/jquery.ui.map.services.min.js"></script>-->

  </head>
  <body >
   
      <div id="map_canvas" style="width:700px; height:600px;float:left;border:1px solid black" ></div>
      <div id="" style="float:left;border: 1px solid;padding:20px;margin-left:30px;width:300px">
          Skyline postcode geotaging module V 1.01
            <form id="marker" method="post" action="{$_subdomain}/Diary/updateGeoTag">
                <input type="hidden" name="postcode" id="postcode" value="{$pc}">
                <input type="hidden" name="lat" id="lang2" value="">
                <input type="hidden" name="lng" id="long2" value="">
          <table>
              <tr>
                  <td> Editing GeoTag for:  </td>
                  <td><span style="font-weight:bolder">{$pc}</span> </td>
              </tr>
              <tr>
                  <td><label for="country">Country</label></td>
                  <td> <span id="country" class="txt" name="country" value=""/></span></td>
              </tr>
              <tr>
                  <td> <label for="state">Town/City</label></td>
                  <td><span id="town" class="txt" name="state" value=""/></span></td>
              </tr>
              <tr>
                  <td> <label for="address">Address</label></td>
                  <td><span id="address" class="txt" name="address" value="" style="width:200px"/></span></td>
              </tr>
              <tr>
                  <td><label for="address">Longitude:</label></td>
                  <td><span type=text id=long></span></td>
              </tr>
          
              <tr>
                  <td> <label for="address">Latitude:</label></td>
                  <td><span type=text id=lang></span></td>
              </tr>
              <tr>
                  <td colspan=2> <input type="submit" value="Save GeoTag" ></td>
               
              </tr>
          
          </form>
           </table>
      </div>

      <script>

function findLocation(location, marker) {
	$('#map_canvas').gmap('search', { 'location': location}, function(results, status) {
		if ( status === 'OK' ) {
			$.each(results[0].address_components, function(i,v) {
                       // console.log(results);
				if ( v.types[0] == "postal_town" ) {
					$('#town').html(v.long_name);
				} else if ( v.types[0] == "country") {
					$('#country').html(v.long_name);
				}
			});
                       
                        //console.log(results[0].geometry.location.ab);
				
					$('#long').html(results[0].geometry.location.ab);
					$('#long2').val(results[0].geometry.location.ab);
					
				
					$('#lang').html(results[0].geometry.location.$a);
					$('#lang2').val(results[0].geometry.location.$a);
				
			
			marker.setTitle(results[0].formatted_address);
			$('#address').html(results[0].formatted_address);
			openDialog(marker);
		}
	});
}

function openDialog(marker) {
	$('#dialog'+marker.__gm_id).dialog({ 'modal':true, 'title': 'Edit and save point', 'buttons': { 
		"Remove": function() {
			$(this).dialog( "close" );
			marker.setMap(null);
		},
		"Save": function() {
			$(this).dialog( "close" );
		}
	}});
}
</script>
  </body>
</html>