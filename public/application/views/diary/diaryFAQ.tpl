{extends "DemoLayout.tpl"}

{block name=config}
{$Title = "Skyline Diary FAQ"}
{$PageId = 89}

    


{/block}
{block name=afterJqueryUI}
   <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
{/block}

{block name=scripts}

     
        <link rel="stylesheet" href="{$_subdomain}/css/contextMenu/jquery.contextMenu.css" type="text/css" charset="utf-8" /> 
       
        <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/jquery.ui.timepicker.css?v=0.3.1" type="text/css" />
        <link rel="Stylesheet" type="text/css" href="{$_subdomain}/css/themes/pccs/jPicker-1.1.6.css" />
        <script type="text/javascript" src="{$_subdomain}/js/jquery.contextMenu.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jspostcode.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jquery.Editable.js"></script>
       
<script type="text/javascript" src="{$_subdomain}/js/ui/jquery.ui.timepicker.js?v=0.3.1"></script>

<script src="{$_subdomain}/js/jpicker-1.1.6.js" type="text/javascript"></script>


<link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
<link href="{$_subdomain}/css/themes/pccs/jquery.multiselect.css" rel="stylesheet" type="text/css" />
    
<script type="text/javascript" src="{$_subdomain}/js/functions.js"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="{$_subdomain}/js/ui/jquery.ui.map.full.min.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/ui/jquery.ui.map.extensions.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/date.js"></script>

   
<style type="text/css" >
      
    .ui-combobox-input {
         width:270px;
        
     }  
     
     #LunchBreakDurationElement input {
         width:235px;
        
     }
         
</style>


<script>
    var oTable;
    var selectedRow;
    $(document).ready(function() {
    /* Init the table */
	oTable = $('#holiday_table').dataTable( {
"sDom": '<"left"lr><"top"f>t<"bottom"><"centered"pi><"clear">',
"bPaginate": true,

"sPaginationType": "full_numbers",
"aLengthMenu": [[ 10, 15, 25, 50, 100 , -1], [10, 15, 25, 50, 100, "All"]],
"iDisplayLength" : 10,
"bPaginate": true,
"aoColumns": [ 
			
			
			             
			  { "bVisible":    false },         
			 null
			 
			  
                            
                            
                            
                            
		] ,
                "aaSorting": [ ]
,
"sEmptyTable": "There are no records"
}

);
      {if $super}
    /* Add a dblclick handler to the rows - this could be used as a callback */
	$("#holiday_table tbody").dblclick(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
            var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
    
     $.post("{$_subdomain}/Diary/showFaqCard",{ update:aData[0] },
function(data) {

$.colorbox({ html:data, title:"Edit Engineers Holidays & Special Appointments",escKey: false,
    overlayClose: false


});


})
    }
    
  
        });
        
        {/if}
        /* Add a click handler for the edit row */
	$('#hEdittBtn').click( function() {
		var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
    
      $.post("{$_subdomain}/Diary/showFaqCard",{ update:aData[0] },
function(data) {

$.colorbox({ html:data, title:"Edit Engineers Holidays & Special Appointments",escKey: false,
    overlayClose: false
,onComplete:function(data){
		
		
	
    
    }

});


})
    }
    
  
        });
        });
        
        
$(document).ready(function() {  
          /* Add a insert button click handler  */
	$("#hInsertBtn").click(function() {
		
	
    
      $.post("{$_subdomain}/Diary/showFaqCard",{ o:false },
function(data) {

$.colorbox({ html:data, title:"Inserting / Editing an FAQ",escKey: false,
    overlayClose: false


});

}
)
    
                
	});
        /* Add a click handler to the rows - this could be used as a callback */
	$("#holiday_table tbody").click(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
                var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
                
    
	});
        
    
        
        });
        /* Get the rows which are currently selected */
function fnGetSelected( oTableLocal )
{
	var aReturn = new Array();
	var aTrs = oTableLocal.fnGetNodes();
	
	for ( var i=0 ; i<aTrs.length ; i++ )
	{
		if ( $(aTrs[i]).hasClass('row_selected') )
		{
			aReturn.push( aTrs[i] );
		}
	}
	return aReturn;
}
function deleteConfirmation(){
 var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
                $('#selectedID').val(aData[0]);
                
if (anSelected!="")  // null if we clicked on title row
    {
$.colorbox({ html:$('#hDeleteMsgDiv').html(), title:"Confirmation",escKey: false,
    overlayClose: false
,onComplete:function(data){
	
    
    }

});
}else{
alert("Please select a appointment to delete.");
}

}

$(document).ready(function() 
{ 
 
}); 

function filterTable(name){
$('input[type="text"]','#holiday_table_filter').val(name);
e = jQuery.Event("keyup");
e.which = 13;
$('input[type="text"]','#holiday_table_filter').trigger(e);


}
function showProcImg(){
$.colorbox({ html:$('#waitDiv2').html(), title:"",escKey: false,
    overlayClose: false});
$('#cboxClose').remove();
}
function setCategory(val){
window.location="{$_subdomain}/Diary/faq/cat="+val;
}
</script>
{/block}




{block name=body}

<div class="breadcrumb">
    <div>
        <a href="{$_subdomain}/index/index">Home</a>/<a href="{$_subdomain}/Diary/">Appointment Diary</a> / FAQ's 
       
    </div>
</div>
    
   <div class="main" id="home">
         <fieldset style="padding:0xp;">
            <legend>Frequently Asked Questions</legend>
            <input type="hidden" name="selectedID" value="" id="selectedID">
            <input type="hidden" name="fDateFilter" value="" id="fDateFilter" style="float:right;position: relative" onchange="">
            <div style="position:relative">
                <div style="position:absolute;top:4px;left:300px">Category: 
                <select onchange="setCategory($(this).val())" style="height:28px">
                <option></option>
                {foreach $categoryList as $ff}
                              <option {if isset($cat)&&$cat==$ff.DiaryFaqCategoryID}selected=selected{/if}   value="{$ff.DiaryFaqCategoryID}">{$ff.CategoryName}</option>
               {/foreach}
            </select>
                    </div>
            </div>
    <table id="holiday_table" border="0" cellpadding="0" cellspacing="0" class="browse" >
                 
        <thead style="display:none">
            <tr>
                <th></th>
                <th></th>
            </tr>
        </thead>
                <tbody>
                  
                      {for $e=0 to $data|@count-1}
                      
                   <tr>
                       <td>
                       {$data[$e]['DiaryFaqID']}
                       </td>
                       <td>
                           <div style="float:left;padding-top:3px;">Category: </div> <div  style="font-weight:bold;font-size:14px;float:left;">&nbsp;{$data[$e]['CategoryName']}</div><br><br>
                           <p onclick="$('#a_{$e}').toggle();">
                               <span style="color:red">
                                   Q: {$data[$e]['FaqQuestion']}
                               </span>
                           </p>
                           <p style="display:none" id="a_{$e}" >
                               <span>
                                   A: {$data[$e]['FaqAnswer']}
                               </span>
                           
                           </p>
                       </td>
                      
                   </tr>
                   
                   {/for}
                       
                 
                </tbody>
             </table>
                   {if $super}
            <div id="holidayButtons">
                <button type="button" id="hInsertBtn" class="btnStandard" style="float:none;color:white;"><span style="color:white">Insert</span></button>
                <button type="button" id="hEdittBtn" class="btnStandard" style="float:none;color:white;"><span style="color:white">Edit</span></button>
                <button type="button" id="hDeleteBtn" class="btnStandard" style="float:none;color:white;" onclick="deleteConfirmation();"><span style="color:white">Delete</span></button>
            </div>
            {/if}
         </fieldset>
    </div>
                   <div style="display:none" id="hDeleteMsgDiv">
                       <fieldset>
                           <p>Are you sure you want to delete this Entry?</p>
                           <div style="margin:auto;">
                           <button type="button" class="gplus-red" style="float:left;color:white; " onclick="showProcImg();window.location='{$_subdomain}/Diary/deleteFaqEntry/'+$('#selectedID').val()"><span style="color:white">Delete</span></button>
                           <button type="button" class="btnStandard" style="float:right;color:white;" onclick="$.colorbox.close();"><span style="color:white">Cancel</span></button>
                           </div>
                       </fieldset>
                   </div>
    
    <div id="waitDiv2" style="display:none">
     <div style="min-height:100px;min-width: 100px;">
        <img src="{$_subdomain}/images/processing.gif">
        <div style="text-align: center">Please wait!</div></div> </div>   
        
{/block}
