{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $ClosedJobsPage}
    {$newjquery = true}
{/block}


{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
    <script src="{$_subdomain}/js/jquery.multiselect.js" type="text/javascript"></script>
    <script src="{$_subdomain}/js/jquery_plugins/jquery.multiselect.filter.js" type="text/javascript"></script>
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
    <link href="{$_subdomain}/css/themes/pccs/jquery.multiselect.css" rel="stylesheet" type="text/css" />
{/block}  


{block name=scripts}

<script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/datatables.api.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/functions.js"></script>

<link href="{$_subdomain}/css/Base/checkbox.css" rel="stylesheet" type="text/css" />

<style>

    li.ui-multiselect-close {
	margin-top: -25px;
	margin-right: 5px;
    }
    
    #scheduledReportFieldset fieldset {
	padding-top: 5px;
    }
    
</style>


<script>


$(document).ready(function() {
    
    var showCheckedOnly = false;
    
    
    $("img").tooltip();
    
    
    $(".multiselect").each(function() {
	var the = this;
	$.post(
	    "{$_subdomain}/Report/getColumnData", 
	    {
		table:	$(the).parent().attr("table"), 
		column: $(the).parent().attr("column"),
		report:	getUrlVars()["id"] ? getUrlVars()["id"] : null
	    },
	    function(response) {
		if(response == "") {
		    return false;
		}
		var data = JSON.parse(response);
		var html = "";
		
		if(data && data.length != 0) {
		
		    for(var i = 0; i < data.length; i++) {
			html += "<option value='" + data[i]["id"] + "'>" + data[i]["col"] + "</option>";
		    }
		    $(html).appendTo(the);
		    
		    $(the).multiselect({
			close: function(event, ui) {
			    getRowsFound();
			},
			open: function(event, ui) {
			    if($(".multiCloseText").length == 0) {
				$("a.ui-multiselect-close").prepend("<span class='multiCloseText'>Save</span>");
			    }
			    //
			    $(".ui-widget-header").css("height", "65px");
			    if (!$("#checkedDiv").length) {
				var html = "<div id='checkedDiv' style='display:block; position:relative; float:left; margin-top:3px;'>\
						<input type='radio' value='checked' name='showCheckedOnly' style='margin-left:0px;' /> Checked only\
						<input type='radio' value='all' name='showCheckedOnly' /> All\
					    </div>\
					   ";
				$(html).appendTo(".ui-widget-header");
			    }
			    if(showCheckedOnly) {
				$("input[name=showCheckedOnly][value=checked]").attr("checked", true);
				hideUnchecked();
			    } else {
				$("input[name=showCheckedOnly][value=all]").attr("checked", true);
				showAll();
			    }
			    //
			},
			create: function(event, ui) {
			    setTimeout(function() {
				var selected =	$(the).multiselect("getChecked").map(function() { 
						    return this.value;
						}).get();
				var totalCount = $(the).find("option").length;
				$($(the).next(".ui-multiselect").find("span:eq(1)")).text(selected.length + " of " + totalCount + " selected");
			    }, 1000);
			},
			selectedText: "# of # selected"
		    }).multiselect("uncheckAll").multiselectfilter({ autoReset: true });
		    
		    var selected = 0;
		    
		    for(var i = 0; i < data.length; i++) {
			if(data[i]["selected"]) {
			    $(the).multiselect("widget").find(":checkbox[value='" + data[i]["id"] + "']").attr("checked", true);
			    selected++;
			}
		    }
		    
		    if(selected > 0) {
			$($(the).next(".ui-multiselect").find("span").get(1)).text(selected + " selected");
		    }
		    
		}
	    }
	);
    });
    
    
    $(document).on("change", "input[name=showCheckedOnly]", function() {
	if($(this).val() == "all") {
	    showCheckedOnly = false;
	    showAll();
	} else {
	    showCheckedOnly = true;
	    hideUnchecked();
	}
    });
    
    
    function hideUnchecked() {
	$(".ui-multiselect-checkboxes li label input").each(function() {
	    if(!$(this).is(":checked")) {
		$(this).parent().parent().hide();
	    }
	});
    }
    
    
    function showAll() {
	$(".ui-multiselect-checkboxes li").each(function() {
	    $(this).show();
	});
    }
    
    
    function loadColumns(table, el) {
    
	$.post("{$_subdomain}/Report/getTableColumns", { tableName: table }, function(response) {
	    var data = $.parseJSON(response);
	    var selected = [];
	    var final = [];
	    $("#selectedColumns li").each(function() {
		selected.push({ table: $(this).attr("table"), column: $(this).attr("column") });
	    });
	    for (var i = 0; i < data.length; i++) {
		remove = false;
		for(var j = 0; j < selected.length; j++) {
		    if(data[i]["SystemName"] == selected[j]["column"] && data[i]["TableName"] == selected[j]["table"]) {
			remove = true;
		    }
		}
		if (!remove) {
		    final.push(data[i]);
		}
	    }
	    var html = "";
	    for (var i = 0; i < final.length; i++) {
		html += "<li column='" + final[i].SystemName + "' table='" + final[i].TableName + "'>" +
			    "<div class='squaredFour'style='display:block; float:left; position:relative; margin:7px 25px 0px -18px;'>" +
				"<input id='" + final[i].SystemName + final[i].TableName + "' type='checkbox' style='display:block; float:left; position:relative; margin-bottom:0; margin-left:0; margin-top:10px;' />" +
				"<label for='" + final[i].SystemName + final[i].TableName + "'></label>" +
			    "</div>" +
			    "<span class='columnName'>" + final[i].ColumnName + "</span>" +
			"</li>";
	    }
	    $("#activeTableColumns").html(html);
	    if (el == null) {
		$("#activeTable").text("Active Table - " + $("input[name=activeTable]:checked").prev(".ui-combobox").find(".ui-combobox-input").val());
	    } else {
		$("#activeTable").text("Active Table - " + $(el).prev(".ui-combobox").find(".ui-combobox-input").val());
	    }
	    document.body.style.cursor = "default";
	});
	
    }
    
    
    $("#primaryTable").combobox({
	change: function(event, ui) {
	    $("#selectedColumns").html("");
	    $("#secondaryTable").next(".ui-combobox").find(".ui-combobox-input").val("");
	    $("#multipleTable").next(".ui-combobox").find(".ui-combobox-input").val("");
	    $("#totalRecordsCount").text("0");
	    if ($("#primaryTable").val() == "job") {
		$("input[name=jobType]").attr("disabled", false);
		$("input[name=jobType][value=all]").attr("checked", true);
	    } else {
		$("input[name=jobType]").attr("disabled", true);
		$("input[name=jobType]").attr("checked", false);
	    }
	    $.post("{$_subdomain}/Report/getSecondaryTables", { tableName: $("#primaryTable").val() }, function(response) {
		var data = $.parseJSON(response);
		var multiOptions = "";
		var secondaryOptions = "";
		for (var i = 0; i < data.multiple.length; i++) {
		    multiOptions += "<option value='" + data.multiple[i].SystemName + "'>" + data.multiple[i].Name + "</option>";
		}
		for (var i = 0; i < data.secondary.length; i++) {
		    secondaryOptions += "<option value='" + data.secondary[i].SystemName + "'>" + data.secondary[i].Name + "</option>";
		}
		$("#multipleTable").html(multiOptions);
		$("#secondaryTable").html(secondaryOptions);
	    });
	    $("input[name=activeTable][value=primary]").attr("checked", true);
	    loadColumns($(this).val(), null);
	}
    });
    
    
    $("#multipleTable").combobox({
	change: function(event, ui) {
	    $("input[name=activeTable][value=multiple]").attr("checked", true);
	    loadColumns($(this).val(), null);
	}
    });
    
    
    $("#secondaryTable").combobox({
	change: function(event, ui) {
	    $("input[name=activeTable][value=secondary]").attr("checked", true);
	    loadColumns($(this).val(), null);
	}
    });
    
    
    function getRowsFound() {
    
	$("#totalRecordsCount").html("<img src='{$_subdomain}/images/spinner-circle.gif' style='margin-top:-3px;' />");
    
	var data = getStructureData();
	
	if (data.columns.length == 0) {
	    $("#totalRecordsCount").text(0);
	    return false;
	}

	$.ajax({
	    type:   "POST",
	    url:    "{$_subdomain}/Report/getReportRowCount",
	    data:   { structure: JSON.stringify(data) }
	}).done(function(response) {
	    if (!response) {
		response = 0;
	    }
	    $("#totalRecordsCount").text(response);
	});
    
    }
    
    
    function getStructureData() {
	
	var data = { };
	data.primaryTable = $("#primaryTable").val();
	data.multiTable = $("#multipleTable").val();
	data.columns = [];
	$("#selectedColumns li").each(function() {
	    var the = this;
	    data.columns.push({
		table:	$(this).attr("table"), 
		column: $(this).attr("column"), 
		name:	$(this).find(".columnName").text(),
		subset: (function() {
		    if($(the).find("select").length == 1 && $(the).find("select").find("option").length > 0) {
			var set =   $(the).find("select").multiselect("getChecked").map(function() { 
					return this.value;
				    }).get();
			var index = set.indexOf("all");
			if(index != -1) {
			    set.splice(index, 1);
			}
			index = set.indexOf("checked");
			if(index != -1) {
			    set.splice(index, 1);
			}
			return set;
		    } else {
			return null;
		    }
		})()
	    });
	});
	data.name = $("#reportName").val();
	data.description = $("#reportDescription").val();
    
	return data;
    
    }
    
    
    function getScheduleData() {
	var schedule = { };
	schedule.jobType = $("input[name=jobType]:checked").val();
	schedule.dateType = $("input[name=dateType]:checked").val();
	schedule.datePeriod = $("input[name=datePeriod]:checked").val();
	schedule.recipients = [];
	$("#recipients div").each(function() {
	    schedule.recipients.push($(this).find("input").val());
	});
	return schedule;
    }
    
    
    function getMultiselect(el) {
    
	el.find("input[type=checkbox]").attr("checked", false);
	el.find(".ui-multiselect").remove();
	$.ajax({
	    url:    "{$_subdomain}/Report/getColumnData", 
	    data:   {
		table:	el.attr("table"), 
		column:	el.attr("column"),
		report:	getUrlVars()["id"] ? getUrlVars()["id"] : null
	    },
	    async:  false
	}).done(function(response) {
	    if(response == "") {
		return false;
	    }
	    var data = JSON.parse(response);
	    if(data && data.length != 0) {
		var html = "<select>";
		for(var i = 0; i < data.length; i++) {
		    html += "<option value='" + data[i]["id"] + "'>" + data[i]["col"] + "</option>";
		}
		html += "</select>";
		$(html).appendTo(el);
		var node = el.find("select");
		node.multiselect({
		    close: function(event, ui) {
			getRowsFound();
		    },
		    open: function(event, ui) {
			if($(".multiCloseText").length == 0) {
			    $("a.ui-multiselect-close").prepend("<span class='multiCloseText'>Save</span>");
			}
			$(".ui-widget-header").css("height", "65px");
			if (!$("#checkedDiv").length) {
			    var html = "<div id='checkedDiv' style='display:block; position:relative; float:left; margin-top:3px;'>\
					    <input type='radio' value='checked' name='showCheckedOnly' style='margin-left:0px;' /> Checked only\
					    <input type='radio' value='all' name='showCheckedOnly' /> All\
					</div>\
				       ";
			    $(html).appendTo(".ui-widget-header");
			}
			if(showCheckedOnly) {
			    $("input[name=showCheckedOnly][value=checked]").attr("checked", true);
			    hideUnchecked();
			} else {
			    $("input[name=showCheckedOnly][value=all]").attr("checked", true);
			    showAll();
			}
		    },
		    selectedText: "# of # selected"
		}).multiselect("checkAll").multiselectfilter({ autoReset: true });
	    }
	});
	
    }
    
    
    function getFilterColumns() {
	
	$.ajax({
	    type:   "POST",
	    url:    "{$_subdomain}/Report/getReportFilterColumns",
	    data:   { data: JSON.stringify(getStructureData()) }
	}).done(function(response) {
	    var data = JSON.parse(response);
	    if (!data) {
		$("#dateTypeFieldset div").remove();
		return false;
	    }
	    var current = [];
	    var newCols = [];
	    for (var i = 0; i < data.length; i++) {
		newCols.push(data[i]["colTable"] + "." + data[i]["colName"]);
	    }
	    $(".dateTypeDiv").each(function() {
		if (newCols.indexOf($(this).attr("col")) == -1) {
		    $(this).remove();
		    return false;
		}
		current.push($(this).attr("col"));
	    });
	    for (var i = 0; i < newCols.length; i++) {
		if (current.indexOf(newCols[i]) == -1) {
		    var html = "<div class='dateTypeDiv' col='" + newCols[i] + "'>\
				    <input type='radio' name='dateType' value='" + newCols[i] + "' /> " + data[i]["structure"]["Name"] + "\
				</div>";
		    $("#dateTypeFieldset").append(html);
		}
	    }
	});
	
    }
    
    
    function showScheduler() {
	if (!$("#scheduledReport").is(":visible")) {
	    $("#scheduledReport").toggle();
	    $("#dateTypeFieldset").css("min-height", $("#jobTypeFieldset").outerHeight() + "px");
	    $("#recipientListFieldset").css("min-height", $("#datePeriodFieldset").outerHeight() + "px");
	    $(".schedule").text("Hide Scheduler");
	    if ($("#primaryTable").val() != "job") {
		$("input[name=jobType]").attr("disabled", true);
		$("input[name=jobType]").attr("checked", false);
	    } else if ($("input[name=jobType]").is(":unchecked")) {
		$("input[name=jobType][value=all]").attr("checked", true);
	    }
	}
	$("html, body").animate({
	    scrollTop: $("#scheduledReport").offset().top
	}, 1000);
    }
    
    
    $("#activeTableColumns, #selectedColumns").sortable({
	connectWith:	".sortable",
	placeholder:	"placeholder",
	revert:		true,
	update: function(event, ui) {
	    if(ui.sender && ui.sender.attr("id") == "activeTableColumns") {
		getMultiselect(ui.item);
		getRowsFound();
		getFilterColumns();
	    }
	    if(ui.sender && ui.sender.attr("id") == "selectedColumns") {
		ui.item.find("input[type=checkbox]").attr("checked", false);
		ui.item.find(".ui-multiselect").remove();
		getRowsFound();
		getFilterColumns();
	    }
	}
    }).disableSelection();
    
    
    $(document).on("mousedown", "input[name=activeTable]", function() {
	if ($(this).prev(".ui-combobox").find(".ui-combobox-input").val()) {
	    document.body.style.cursor = "wait";
	    loadColumns($(this).prev().prev().val(), this);
	} else {
	    alert("You have to select the table from the dropdown first.");
	    return false;
	}
    });
    
    
    $(document).on("click", ".save", function() {
	
	if ($("#reportName").val() == "") {
	    alert("You must enter Report Name first.");
	    return false;
	}
	
	if ($("#selectedColumns").find("li").length == 0) {
	    alert("You must select at least one field.");
	    return false;
	}
	
	if ($("input[name=schedulerStatus]:checked").val() == "on") {
	    if (!$("input[name=dateType]").is(":checked")) {
		showScheduler();
		alert("You must select Date Type first.");
		return false;
	    }
	    if (!$("input[name=datePeriod]").is(":checked")) {
		showScheduler();
		alert("You must select Date Period first.");
		return false;
	    }
	    if ($("#recipients").find("div").length == 0) {
		showScheduler();
		alert("You must have at least one recipient.");
		return false;
	    }
	}
	
	document.body.style.cursor = "wait";
	
	var data = getStructureData();
	var schedule = getScheduleData();
	var scheduleStatus = $("input[name=schedulerStatus]:checked").val() ? $("input[name=schedulerStatus]:checked").val() : "off";
	
	$.ajax({
	    type:   "POST",
	    url:    "{$_subdomain}/Report/saveUserReport",
	    data: { 
		data:		JSON.stringify(data),
		id:		{if isset($id)}{$id}{else}null{/if},
		schedule:	JSON.stringify(schedule),
		scheduleStatus:	scheduleStatus
	    }
	}).done(function(response) {
	    location.href = "{$_subdomain}/Report/generator";
	});	
	
	return false;
    });
    
    
    $(document).on("click", ".cancel", function() {
	location.href = "{$_subdomain}/Report/generator";
    });
    
    
    $(document).on("click", "#clickToAdd", function() {
	var move = [];
	$("#activeTableColumns li").each(function() {
	    if($(this).find("input").is(":checked")) {
		$(this).find(".ui-multiselect, select").remove();
		getMultiselect($(this.outerHTML).appendTo($("#selectedColumns")));
		$(this).remove();
	    }
	});
	getRowsFound();
	return false;
    });
    
    
    $(document).on("click", "#clickToRemove", function() {
	var move = [];
	$("#selectedColumns li").each(function() {
	    if($(this).find("input").is(":checked")) {
		$(this).find(".ui-multiselect, select").remove();
		$(this.outerHTML).appendTo($("#activeTableColumns"));
		$(this).remove();
	    }
	});
	getRowsFound();
	return false;
    });
    
    
    $(document).on("click", ".schedule", function() {
	$("#scheduledReport").toggle();
	if ($("#scheduledReport").is(":visible")) {
	    $("#dateTypeFieldset").css("min-height", $("#jobTypeFieldset").outerHeight() + "px");
	    $("#recipientListFieldset").css("min-height", $("#datePeriodFieldset").outerHeight() + "px");
	    $(".schedule").text("Hide Scheduler");
	    if ($("#primaryTable").val() != "job") {
		$("input[name=jobType]").attr("disabled", true);
		$("input[name=jobType]").attr("checked", false);
	    } else if (!$("input[name=jobType]").is(":checked")) {
		$("input[name=jobType][value=all]").attr("checked", true);
	    }
	    $("html, body").animate({
		scrollTop: $("#scheduledReport").offset().top
	    }, 1000);
	} else {
	    $(".schedule").text("Show Scheduler");
	}
	return false;
    });
    
    
    $(document).on("click", "#addRecipient", function() {
	var html = "<div>\
			<input type='text' value='' style='width:92%;' />\
			<a href='#' class='deleteReciptient' style='display:block; float:right; position:relative; margin-top:5px;'>\
			    <img src='{$_subdomain}/images/red_cross.png' />\
			</a>\
		    </div>\
		   ";
	$("#recipients").append(html);
	return false; 
    });
    
    
    $(document).on("click", ".deleteReciptient", function() {
	$(this).parent().remove();
	return false;
    });
    
    
    $(document).on("click", ".helpIcon", function() {
	var the = this;
	$.ajax({
	    type:   "POST",
	    url:    "{$_subdomain}/Report/getHelpInfo",
	    data:   { code: $(the).attr("id") }
	}).done(function(response) {
	    var data = JSON.parse(response);
	    if (data) {
		var html = "<fieldset>\
				<legend id='helpInfoLegend'>" + data["title"] + "</legend>\
				<div style='margin:10px 0px;'>" + data["text"] + "</div>\
				<div style='text-align:center'><a href='#' style='width:60px;' class='btnCancel'>Close</a></div>\
			    </fieldset>\
			   ";
		{if $loggedin_user->UserType == "Admin"}
		    var html = "<fieldset>\
				    <legend id='helpInfoLegend'>" + data["title"] + "</legend>\
				    <div style='margin:10px 0px;' id='helpInfoTextDiv'>" + data["text"] + "</div>\
				    <div style='text-align:center'>\
					<a href='#' style='width:60px; margin-right:10px;' code='" + $(the).attr("id") + "' id='editHelpInfo' class='btnStandard'>Edit</a>\
					<a href='#' style='width:60px; margin-right:10px; display:none;' code='" + $(the).attr("id") + "' id='saveHelpInfo' class='btnConfirm'>Save</a>\
					<a href='#' style='width:60px;' class='btnCancel'>Close</a>\
				    </div>\
				</fieldset>\
			       ";
		{/if}
		$.colorbox({
		    html :	html,
		    width:	"600px",
		    scrolling:	false,
		    onComplete: function() {
		    },
		    onClosed: function() {
		    }
		});
	    } else {
		return false;
	    }
	});	
	return false;
    });
    
    
    $(document).on("click", ".btnCancel", function() {
	$.colorbox.close();
	return false;
    });
    
    
    {if $loggedin_user->UserType == "Admin"}
    
	$(document).on("click", "#editHelpInfo", function() {
	    $("#saveHelpInfo").show();
	    $("#editHelpInfo").hide();
	    var html = "<label for='helpInfoTitle' style='float:left;'>Title:</label>\
			<input type='text' name='helpInfoTitle' style='width:100%;' value='" + $("#helpInfoLegend").text() + "' />\
			<label for='helpInfoText' style='clear:both; margin-top:5px;'>Text:</label>\
			<textarea name='helpInfoText' style='width:100%; height:150px;'>" + $("#helpInfoTextDiv").text() + "</textarea>\
		       ";
	    $("#helpInfoTextDiv").html(html);
	    $("#helpInfoLegend").text("Edit Help Info");
	    $.colorbox.resize();
	    return false;
	});


	$(document).on("click", "#saveHelpInfo", function() {
	    var the = this;
	    if ($("input[name=helpInfoTitle]").val() == "") {
		alert("Title cannot be empty.");
		return false;
	    }
	    if ($("textarea[name=helpInfoText]").val() == "") {
		alert("Text cannot be empty.");
		return false;
	    }
	    document.body.style.cursor = "wait";
	    $.ajax({
		type:   "POST",
		url:    "{$_subdomain}/Report/saveHelpInfo",
		data: {
		    title: $("input[name=helpInfoTitle]").val(),
		    text: $("textarea[name=helpInfoText]").val(),
		    code: $(the).attr("code")
		}
	    }).done(function(response) {
		$.colorbox.close();
		document.body.style.cursor = "default";
	    });	
	});
    
    {/if}
    
});


</script>
    
{/block}


{block name=body}

    
<div class="breadcrumb">
    <div>
        <a href="{$_subdomain}/index/index">{$page['Text']['home_page']|escape:'html'}</a> / <a href="{$_subdomain}/Report/generator">{$page['Text']['page_title']|escape:'html'}</a> / &nbsp;{$page['Text']['create_report_title']|escape:'html'}
    </div>
</div>

{include file='include/menu.tpl'}


<div id="wrapper" style="display:block; position:relative; float:left; margin-top:30px; width:100%;">
    
    {*
    <fieldset style="margin-bottom:20px;">
	<legend>Report Generator</legend>
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis urna tortor, egestas in rutrum eget, dignissim ut metus. 
	    Donec vitae risus non justo convallis fringilla a at leo. Cras sagittis. 
	</p>
    </fieldset>
    *}

    <div id="reportTables" style="position:relative; display:block; float:left;">
	
	<div id="primaryDiv" style="display:block; position:relative; height:33px; padding-right:33px;">
	    <label for="primaryTable" style="width:200px; text-align:left; margin-right:5px;">Primary Table:</label>
	    <a href="#" class="helpIcon" id="ReportPrimaryTableInfo" style="display:inline-block; width:18px;">
		<img src="{$_subdomain}/images/information_icon.png" style="height:18px; position:absolute; display:block; top:6px;" />
	    </a>
	    <select name="primaryTable" id="primaryTable">
		<option value="">Select Table</option>
		{foreach $primaryTables as $table}
		    <option value="{$table.SystemName}" {if isset($primaryTable) && $primaryTable == $table.SystemName}selected="selected"{/if}>{$table.Name}</option>
		{/foreach}
	    </select>
	    <input type="radio" name="activeTable" value="primary" style="margin-left:10px;" />
	    <a href="#" id="primaryTableSettings" style="display:inline-block; position:absolute; right:0px; top:-1px;" class="tableSettings"><img src="{$_subdomain}/images/wrench.png" /></a>
	</div>
	
	<div id="secondaryDiv" style="display:block; position:relative; height:33px;">
	    <label for="secondaryTable" style="width:200px; text-align:left; margin-right:5px;">Individual Sub Record Table:</label>
	    <a href="#" class="helpIcon" id="ReportSecondaryTableInfo" style="display:inline-block; width:18px;">
		<img src="{$_subdomain}/images/information_icon.png" style="height:18px; position:absolute; display:block; top:6px;" />
	    </a>
	    <select name="secondaryTable" id="secondaryTable">
		<option value="">Select Table</option>
		{if isset($secondaryTables)}
		    {foreach $secondaryTables["secondary"] as $table}
			<option value="{$table.SystemName}">{$table.Name}</option>
		    {/foreach}
		{/if}
	    </select>
	    <input type="radio" name="activeTable" value="secondary" style="margin-left:10px;" />
	    <a href="#" id="secondaryTableSettings" style="display:inline-block; position:absolute; right:0px; top:-1px;" class="tableSettings"><img src="{$_subdomain}/images/wrench.png" /></a>
	</div>
	
	<div id="multipleDiv" style="display:block; position:relative; height:33px;">
	    <label for="multipleTable" style="width:200px; text-align:left; margin-right:5px;">Multiple Sub Record Table:</label>
	    <a href="#" class="helpIcon" id="ReportMultipleTableInfo" style="display:inline-block; width:18px;">
		<img src="{$_subdomain}/images/information_icon.png" style="height:18px; position:absolute; display:block; top:6px;" />
	    </a>
	    <select name="multipleTable" id="multipleTable">
		<option value="">Select Table</option>
		{if isset($secondaryTables)}
		    {foreach $secondaryTables["multiple"] as $table}
			<option value="{$table.SystemName}" {if isset($multiTable) && $multiTable == $table.SystemName}selected="selected"{/if}>{$table.Name}</option>
		    {/foreach}
		{/if}
	    </select>
	    <input type="radio" name="activeTable" value="multiple" style="margin-left:10px;" />
	    <a href="#" id="multiTableSettings" style="display:inline-block; position:absolute; right:0px; top:-1px;" class="tableSettings"><img src="{$_subdomain}/images/wrench.png" /></a>
	</div>
	
    </div>
	    
    <div id="reportInfoDiv" style="position:relative; float:right;">
	
	<label for="reportName" style="width:140px; text-align:left; margin-right:5px; height:25px; padding-top:5px; vertical-align:middle;">Report Name:</label>
	<input type="text" value="{if isset($reportName)}{$reportName}{/if}" name="reportName" id="reportName" style="width:260px; display:block; position:relative; float:right;" />
	
	<br/>
	
	<label for="reportDescription" style="width:140px; text-align:left; position:relative; height:25px; padding-top:5px; vertical-align:middle; float:left; margin-right:9px;">Report Description:</label>
	<textarea name="reportDescription" cols="20" rows="2" style="height:62px; width:260px;" id="reportDescription">{if isset($reportDescription)}{$reportDescription}{/if}</textarea>
	
    </div>
	
    <div style="margin-top:20px; text-align:center; position:relative; float:left; clear:both; width:100%;">
	<a href="#" class="btnConfirm save" style="width:60px; margin-right:10px;">Save</a>
	<a href="#" class="btnCancel cancel" style="width:60px;">Cancel</a>
	<a href="#" class="btnStandard schedule" style="width:120px; display:block; position:absolute; top:0px;">Show Scheduler</a>
    </div>
	
    <div id="reportColumns" style="position:relative; float:left; clear:both; width:100%; margin-top:20px; text-align:center;">
	
	<fieldset id="activeTableFieldset" style="position:relative; float:left; width:40%; margin-top:0; min-height:50px;">
	    <legend id="activeTable" align="left">Active Table</legend>
	    <ul id="activeTableColumns" class="sortable" style="min-height:30px;">
	    </ul>
	</fieldset>
	
	<div id="middleDiv" style="display:inline-block; margin-top:35px;">
	    <span style="background-image:url('{$_subdomain}/images/arrow_right.png'); font-size:11px; vertical-align:middle; height:20px; color:white; width:100px; display:inline-block; margin-bottom:5px;">
		<a href="#" id="clickToAdd">
		    <span style="display:inline-block; color:#fff; margin-top:2px;">Click to add</span>
		</a>
	    </span>
		<br/>
	    <span style="background-image:url('{$_subdomain}/images/arrow_left.png'); font-size:11px; vertical-align:middle; height:20px; color:white; width:100px; display:inline-block;">
		<a href="#" id="clickToRemove">
		    <span style="display:inline-block; margin-top:2px; color:#fff; margin-left:10px;">Click to remove</span>
		</a>
	    </span>
	</div>
	
	<fieldset id="selectedColumnsFieldset" style="position:relative; height:100%; float:right; width:40%; margin-top:0; min-height:50px;">
	    <legend align="left">Selected Fields</legend>
	    <span id="totalRecords" style="position:absolute; display:block; right:10px; top:3px; background:#FFFFFF; padding:0px 10px;">
		<span id="totalRecordsCount">{if isset($rowCount)}{$rowCount}{else}0{/if}</span> Rows Found
	    </span>
	    <ul id="selectedColumns" class="sortable" style="min-height:30px;">
		{if isset($columns)}
		    {foreach $columns as $column}
			<li column="{$column->column}" table="{$column->table}">
			    <div class="squaredFour" style="display:block; float:left; position:relative; margin:7px 25px 0px -18px;;">
				<input id="{$column->column}{$column->table}" type="checkbox" style="display:block; float:left; position:relative; margin-bottom:0; margin-left:0; margin-top:10px;" />
				<label for="{$column->column}{$column->table}"></label>
			    </div>
			    <span class="columnName">{$column->name}</span>
			    {if $column->subset}
				<select class="multiselect" style="display:none;">
				</select>
			    {/if}
			</li>
		    {/foreach}
		{/if}
	    </ul>
	</fieldset>
	
    </div>
    
    <div style="margin-top:20px; text-align:center; position:relative; float:left; clear:both; width:100%;">
	<a href="#" class="btnConfirm save" style="width:60px; margin-right:10px;">Save</a>
	<a href="#" class="btnCancel cancel" style="width:60px;">Cancel</a>
	<a href="#" class="btnStandard schedule" style="width:120px; display:block; position:absolute; top:0px;">Show Scheduler</a>
    </div>
    
    <div id="scheduledReport" style="display:none; position:relative; float:left; clear:both; width:100%; margin-top:20px;">
	<fieldset id="scheduledReportFieldset">
	    <legend>Scheduled Report</legend>
	    <div style="margin-top:10px; margin-bottom:10px;">
		<input type="radio" name="schedulerStatus" value="on" {if isset($schedule->status) && $schedule->status == "on"}checked="checked"{/if} /> On
		<input type="radio" name="schedulerStatus" value="off" {if isset($schedule->status) && $schedule->status == "off"}checked="checked"{/if} /> Off
	    </div>
	    <div style="display:block; position:relative; float:left; clear:both; width:100%;">
		<fieldset id="jobTypeFieldset" style="position:relative; display:block; width:49.41%; float:left; margin-right:10px;">
		    <legend style="position:relative;">
			Job Type 
			<img src="{$_subdomain}/images/info.jpg" style="display:inline-block; margin-bottom:-2px;" title="Active only when Primary Table is 'Job'" />
		    </legend>
		    <input type="radio" name="jobType" value="open" {if isset($schedule->data->jobType) && $schedule->data->jobType == "open"}checked="checked"{/if} /> Open Jobs <br/>
		    <input type="radio" name="jobType" value="closed" {if isset($schedule->data->jobType) && $schedule->data->jobType == "closed"}checked="checked"{/if} /> Closed Jobs <br/>
		    <input type="radio" name="jobType" value="all" {if isset($schedule->data->jobType) && $schedule->data->jobType == "all"}checked="checked"{/if} /> All Jobs <br/>
		</fieldset>
		<fieldset id="dateTypeFieldset" style="position:relative; display:block; width:49.41%; float:left;">
		    <legend>Date Type</legend>
		    {if isset($filterColumns)}
			{foreach $filterColumns as $column}
			    <div class="dateTypeDiv" col="{$column["colTable"]}.{$column["colName"]}">
				<input type="radio" name="dateType" value="{$column["colTable"]}.{$column["colName"]}" {if isset($schedule->data->dateType) && $schedule->data->dateType == $column["colTable"]|cat:"."|cat:$column["colName"]}checked="checked"{/if} /> {$column["structure"]->Name}
			    </div>
			{/foreach}
		    {/if}
		</fieldset>
	    </div>
	    <div style="display:block; position:relative; float:left; clear:both; width:100%;">
		<fieldset id="recipientListFieldset" style="position:relative; display:block; width:49.41%; float:left; margin-right:10px;">
		    <legend>Recipient List</legend>
		    <a href="#" id="addRecipient" class="btnStandard">Add recipient</a>
		    <div id="recipients" style="margin-top:10px;">
			{if isset($schedule->data->recipients)}
			    {foreach $schedule->data->recipients as $recipient}
				<div>
				    <input type="text" value="{$recipient}" style="width:92%;" />
				    <a href="#" class="deleteReciptient" style="display:block; float:right; position:relative; margin-top:5px;">
					<img src="{$_subdomain}/images/red_cross.png" />
				    </a>
				</div>
			    {/foreach}
			{/if}
		    </div>
		</fieldset>
		<fieldset id="datePeriodFieldset" style="position:relative; display:block; width:49.41%; float:left;">
		    <legend>Date Period</legend>
		    <input type="radio" name="datePeriod" value="daily" {if isset($schedule->data->datePeriod) && $schedule->data->datePeriod == "daily"}checked="checked"{/if} /> Daily <br/>
		    <input type="radio" name="datePeriod" value="weekly" {if isset($schedule->data->datePeriod) && $schedule->data->datePeriod == "weekly"}checked="checked"{/if} /> Weekly <br/>
		    <input type="radio" name="datePeriod" value="monthly" {if isset($schedule->data->datePeriod) && $schedule->data->datePeriod == "monthly"}checked="checked"{/if} /> Monthly <br/>
		</fieldset>
	    </div>
	</fieldset>
    </div>
	    
</div>
    
                
{/block}
