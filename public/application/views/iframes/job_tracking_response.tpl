{extends "iframes/MasterLayout.tpl"}


{block name=config}
{$Title = 'Skyline Job Tracking'}
{/block}
  
{block name=scripts}
<script>
$(document).on('click', '#print-button', function() {
        $('#job-tracking').hide();
        $('#print-panel').show();
	return false; 
    });
$(document).on('click', '#print', function() {
        var printType = $("input[name=printRadio]:radio:checked").val();
        var url = "{$_subdomain}/Job/printJobRecord/" + {$JobID} + "/" + printType + "/?pdf=1";
        $('#print-panel').hide();
        $('#job-tracking').show();
        window.open(url, "_blank");
        window.focus();
	return false; 
    });
$(document).on('click', '#cancel', function() {
        $('#print-panel').hide();
        $('#job-tracking').show();
	return false; 
    });
$(document).on('click', '.pager-submit', function() {
    if (!$(this).hasClass('pager-button-active')) {
        $('#form_job_no').val($(this).attr('data-job'));
        $('#form_job_index').val($(this).attr('data-index'));
        $('#trackingform').submit();
        return false;
    }

    });  
    
$(document).ready(function() {
 $('#prev').mouseover(function(){ $(this).attr('src','{$_subdomain}/iframe{$_skin}/images/prev_hot.png'); })
           .mouseout(function(){ $(this).attr('src','{$_subdomain}/iframe{$_skin}/images/prev.png'); });
   
 $('#next').mouseover(function(){ $(this).attr('src','{$_subdomain}/iframe{$_skin}/images/next_hot.png'); })
           .mouseout(function(){ $(this).attr('src','{$_subdomain}/iframe{$_skin}/images/next.png'); });
} );
</script>
{/block}

{block name=body}
    <div id="job-tracking">
        <div class="sectitle">Job Status Update</div>
        {if $Status eq 'OK'}   
        <p class="confirm-item">
            <span class="label">{if $_skin eq ''}Swann {/if}Job Number:</span>
            <span class="data">{$JobID}</span>
        </p>
        <p class="section-break" />
        <p class="confirm-item">
            <span class="label">Booked Date:</span>
            <span class="data">{$Job.date_booked|date_format: '%d.%m.%Y'}</span>
        </p>
        <p class="section-break" />
        <p class="confirm-item">
            <span class="label">Current Status: </span>
            <span class="data">{$Job.current_status|upper|escape:'html'}</span>
        </p>
        {if isset($Appointment)}
        <p class="section-break" />
        <p class="confirm-item">
            <span class="label">Appointment Date:</span>
            <span class="data">{$Appointment|upper|escape:'html'}</span>
        </p>
        {/if}
        <p class="section-break" />
        <p class="confirm-item">
            <span class="label">Customer Name:</span>
            <span class="data">{$Customer.title|upper|escape:'html'} {$Customer.first_name|upper|escape:'html'} {$Customer.last_name|upper|escape:'html'}</span>
        </p>
        <p class="section-break" />
        <p class="confirm-item">
            <span class="label">Service Required:</span>
            <span class="data">{$Job.service_type|upper|escape:'html'}</span>
        </p>
        <p class="section-break" />
        <p class="confirm-item">
            <span class="label">Manufacturer:</span>
            <span class="data">{$Job.manufacturer|upper|escape:'html'}</span>
        </p>
        <p class="section-break" />
        <p class="confirm-item">
            <span class="label">Product Type:</span>
            <span class="data">{$Job.unit_type|upper|escape:'html'}</span>
        </p>
        <p class="section-break" />
        <p class="info">
            If you have any questions regarding your appointment please contact the allocated Service Centre detailed below:
        </p>
        <div class="sectitle">Allocated Service Centre</div>
        <p class="confirm-address">
            {$ASC.name|upper|escape:'html'}<br />                     
            {if !empty($ASC.address.line_1)}{$ASC.address.line_1|upper|escape:'html'}<br />{/if}
            {if !empty($ASC.address.line_2)}{$ASC.address.line_2|upper|escape:'html'}<br />{/if}
            {if !empty($ASC.address.line_3)}{$ASC.address.line_3|upper|escape:'html'}<br />{/if}
            {if !empty($ASC.address.line_4)}{$ASC.address.line_4|upper|escape:'html'} {/if}{$ASC.address.postcode}<br />
            <br />
            {$ASC.phone|upper|escape:'html'}{if $ASC.ext ne ''} Ext: {$ASC.ext|upper|escape:'html'}{/if}<br />
            <a href="mailto:{$ASC.email|escape:'html'}">{$ASC.email|escape:'html'}</a></span>
        </p>
        {if isset($joblist)}
        <!-- <div id="button-bar">
            <span class="pager">
                {if $job_index > 0}
                <a id="prev-button" class="pager-submit"  href="#" class="pager-submit" data-index="{$job_index-1}" data-job="{$joblist[$job_index-1]}"><img src="{$_subdomain}/iframe{$_skin}/images/prev.png" id="prev"></a>
                {else}
                <img src="{$_subdomain}/iframe{$_skin}/images/prev_none.png">
                {/if}
                {if $job_index < count($joblist)-1}
                <a id="next-button" class="pager-submit" href="" class="pager-submit" data-index="{$job_index+1}" data-job="{$joblist[$job_index+1]}"><img src="{$_subdomain}/iframe{$_skin}/images/next.png" id="next"></a>
                {else}
                <img src="{$_subdomain}/iframe{$_skin}/images/next_none.png">
                {/if}
            </span>
            {* <a id="print-button" class="form-button" style="width: 122px;" href="#">Print</a> *}
        </div> -->
        <div class="pager-buttons">
            {if $job_index > 0}
            <a id="prev-button" class="pager-submit"  href="#" data-index="{$job_index-1}" data-job="{$joblist[$job_index-1]}"><img src="{$_subdomain}/iframe{$_skin}/images/prev.png" id="prev"></a>
            {else}
            <img src="{$_subdomain}/iframe{$_skin}/images/prev_none.png">
            {/if}
            <span class="pager">
            {foreach $joblist as $jobno}
            <span class="pager-submit pager-button{if $job_index eq $jobno@index} pager-button-active{/if}" data-index="{$jobno@index}" data-job="{$jobno}">{$jobno@iteration}</span>
            {/foreach}
            </span>
            {if $job_index < count($joblist)-1}
            <a id="next-button" class="pager-submit" href="" data-index="{$job_index+1}" data-job="{$joblist[$job_index+1]}"><img src="{$_subdomain}/iframe{$_skin}/images/next.png" id="next"></a>
            {else}
            <img src="{$_subdomain}/iframe{$_skin}/images/next_none.png">
            {/if}
        </div>
            <form id="trackingform" name="trackingform" action="{$_subdomain}/IFrame/JobTrackingForm" method="post">
                <input type="hidden" name="joblist" value="{foreach $joblist as $job}{if not $job@first},{/if}{$job}{/foreach}" />
                <input id="form_job_no" type="hidden" name="jobno" />
                <input id="form_job_index" type="hidden" name="job_index" />
            </form> 
        {else}
        <br /><br />
        {/if}
        {else}
        <p>Sorry there was a problem with your job tracking</p
        <br /><br />
        {/if}
    </div>
        
    <div id="print-panel" class="job-booking-response" style="display: none;">
        <h1>Booking Confirmation</h1>
        <fieldset>
            <legend align='center'>Print Options</legend>
            <br />
            <input name='printRadio' type='radio' value ='2' checked /> Print Customer Receipt
            <br />
            <input name='printRadio' type='radio' value ='1' /> Print Job Card
            <br />
            <input name='printRadio' type='radio' value ='3' /> Print Service Report
            <br />
            <br />
            <div style='text-align:center;'>
                <a href='#' id='print' style='width:50px;' class='form-button'>Print</a>&nbsp;
                <a href='#' id='cancel' style='width:50px;' class='form-button'>Cancel</a>
            </div>
        </fieldset>
        <br /><br />
    </div>
{/block}