<script>

$.validator.addMethod('regex', function(value, element, param) {
    return this.optional(element) || value.match(typeof param == 'string' ? new RegExp(param) : param);
    },
    'Please enter a value in the correct format.'
);

$(document).ready(function() {

$('#ModelFilter').combobox();

{if $mode=='update'}
    checkAvailableQuantity();
     $('#Quantity').attr('readonly','readonly');
{/if}
    
setSundryPart();//check if sundry part

setCompulsory();//check if warranty
{if $stockAvailable&&$mode=='new'}
$('#stockSearchContainer').show();
$('#updatePartModalContainer').hide();
{else}
$('#stockSearchContainer').hide();
$('#updatePartModalContainer').show();  
{/if}
    


   
$.colorbox.resize();

//set from visibility
{if $mode=="codeOnly"}
    $('#block_1').hide();
    $('#block_2').hide();
    $('#block_4').hide();
    $('#spacer3').hide();
    $('#IsAdjustment').attr('checked',true);
    $('#PartDescription').val('Adjustment Only');
    $('#PartNo').val('ADJ');
    $('#Quantity').val('1');
    $.colorbox.resize();
    
{/if}
{if $serviceType==2}
   $('#block_2').hide(); 
 {/if}
$.post("{$_subdomain}/Job/checkIfOrderSection", { ID: '{$data.PartStatusID}' }, function(response) {
if(response=="Yes"){
$('#block_4').show();
$.colorbox.resize();
}else{
$('#block_4').hide();
$.colorbox.resize();
}
});
//end set from visibility


$("#SupplierID").combobox();
$("#PartStatusID").combobox({
selected: function(event, ui) {
         $.post("{$_subdomain}/Job/checkIfOrderSection", { ID: $(this).val() }, function(response) {
if(response=="Yes"){
$('#block_4').show();
$.colorbox.resize();
}else{
$('#block_4').hide();
$.colorbox.resize();
}
});
         } // selected
})


     $('#PartStatusID').next(".ui-combobox").children('.ui-autocomplete-input').val($("#PartStatusID option:selected").text()).attr('readonly','readonly');
      $('#PartStatusID').next(".ui-combobox").children('.ui-combobox-toggle').hide();
     
  //check if warranty job
  
  
  
 
    var oTable2 = $('#partsResults').dataTable( {
"sDom": 'R<"left"><"top"f>t<"bottom"><"centered"><"right">pli<"clear">',
//"sDom": 'Rlfrtip',
"bServerSide": true,

		 "oLanguage": {
                                
                                "sSearch": "Search Stock"
                            },
    "sAjaxSource": "{$_subdomain}/StockControl/loadSPPartsList/modelID="+$('#modelFilter').val()+"/chargeType={$jobChargeType}/",
     
                "fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( { "name": "more_data", "value": "my_value" } );
			$.getJSON( sSource, aoData, function (json) { 
				/* Do whatever additional processing you want on the callback, then tell DataTables */
				fnCallback(json)
                               
                                $('#partsResults').show();
                                $.colorbox.resize();
                        
			} );
                        },
                        
                            
  "Paginate": true,
"sPaginationType": "full_numbers",
"aLengthMenu": [[ 20, 25, 50, 100 , -1], [20, 25, 50, 100, "All"]],                      

"iDisplayLength" : 20,

"aoColumns": [ 
			
			
			
                               
                           
                           
			 
                      
                                 { "bVisible":0 },
                               true,
                               true,
                               
                               {if $jobChargeType=="m"||$jobChargeType=="w"} { "bVisible":1 },{else} { "bVisible":0 },{/if}
                               {if $jobChargeType=="m"||$jobChargeType=="c"} { "bVisible":1 },{else} { "bVisible":0 },{/if}
                              
                               true,
                               true
                              
                            
		],
                "bFilter": true
               
   
 
        
          
});//datatable end
  console.log($('#modelFilter').val());
  
  $('input[type="text"]','#partsResults_filter').css({ 'width':340}); 
   $('input[type="text"]','#partsResults_filter').attr({ 'placeholder':'Start Typing Name To Filter Results'});
    

    

   
       


});

function setCompulsory(){

if($('#IsAdjustment').attr('checked')=='checked'){
$('.compy').show();
$('#ChargType').val("W");
}else{

$('.compy').hide();
$('#ChargType').val("C");
}
}

 function savePartsStock(){
{literal}
        var pattern = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
    {/literal}
	$("#updatePartForm").validate = null;
	
	$("#updatePartForm").validate({
	    rules: {
                       
                        SectionCodeID:{
                             required: function (element) {
                                         if($("#IsAdjustment").attr('checked')!='checked')
                                         {
                                             var e = document.getElementById("SectionCodeID");
                                             if(e.options[e.selectedIndex].value=="")
                                             {
                                                return false;  
                                             } 
                                             else
                                             {
                                                return true;
                                             } 
                                         }
                                         else
                                         {
                                             return true;
                                         }  
                                      }  
                                  },
                        DefectCodeID:{
                             required: function (element) {
                                         if($("#IsAdjustment").attr('checked')!='checked')
                                         {
                                             var e = document.getElementById("DefectCodeID");
                                             if(e.options[e.selectedIndex].value=="")
                                             {
                                                return false;  
                                             } 
                                             else
                                             {
                                                return true;
                                             } 
                                         }
                                         else
                                         {
                                             return true;
                                         }  
                                      }  
                                  },
                        RepairCodeID:{
                             required: function (element) {
                                         if($("#IsAdjustment").attr('checked')!='checked')
                                         {
                                             var e = document.getElementById("RepairCodeID");
                                             if(e.options[e.selectedIndex].value=="")
                                             {
                                                return false;  
                                             } 
                                             else
                                             {
                                                return true;
                                             } 
                                         }
                                         else
                                         {
                                             return true;
                                         }  
                                      }  
                                  }

                    },
	   
	    errorPlacement: function(error, element) {
		error.insertBefore(element);
                $.colorbox.resize();
                
	    },
	    ignore:	    "",
	    errorClass:	    "fieldError2",
	    onkeyup:	    false,
	    onblur:	    false,
	    errorElement:   "span",
	    submitHandler: function() {
            //checking available quantity
            $.post("{$_subdomain}/Stock/getStockItemQty/id="+$('#SpPartStockTemplateID').val(),{ },
		    function(data) {
                    en=jQuery.parseJSON(data);
                               console.log(en);          
                    var oldInstock=$('#instockval').val();
                    
                    $('#instockval').val(en);
                     {$upd=$data.Quantity|default:'0'}
if(oldInstock!=$('#instockval').val()&&$('#IsOtherCost').attr('checked')!=true ){
                  
                   
                        
                        $('#chnaged_stockDiv').dialog(
{       
      title:"Warning",
      resizable:false,
      modal: true,
      buttons: {
        Ok:{  text:"Ok","class":"gplus-blue", click:function() {
          $( this ).dialog( "close" );
        }}
      }
    
    }
);
                        
                       
                     
                        $('#Quantity').focus();
                   
                       
                   }
                   else
                   {
   if(($('#instockval').val()*1< $('#Quantity').val()*1-{$upd})){
   ///not enought stock need order
   
                    {if $spInfo.ChangeSupplieronJobPartOrder=='Yes'} //need show change supplier form
                        if($("#IgnoreValidation").val()==""){ //part order form not yet filled
                        
                        $('#genError_stockDiv').html("<p>There are insufficient items in stock</p><p>Do you wish to ORDER the remaining items, or RE-Enter the quantity required?</p>").dialog(
{       
      title:"Insufficient Items",
      resizable:false,
      modal: true,
      buttons: {
      "Order":{ 
       "class": 'gplus-blue w100px',
       text:"Order",
      
       click: function() {
       try {
$( this ).dialog( "close" );
} catch(err){

        }
        
  
    $('#updatePartModalContainer').hide();
    $('#orderHolderDiv').show();
    var qtyy=$('#Quantity').val()*1-$('#instockval').val()*1;
    $('#orderHolderDiv').load('{$_subdomain}/StockControl/orderStockForm/id='+$('#SpPartStockTemplateID').val()+"/mode=2/qty="+qtyy+"/", function() {
    $.colorbox.resize();
    
});
    
         
         
        }
        },
        "ReEnter":{ 
        "class": 'gplus-blue w100px',
        text:"Re-Enter",
        
        click: function() {
        $( this ).dialog( "close" );
          $('#Quantity').focus().select();
          
        }}
      }
      
    }
);
                   return false; 
                   }else{ //part order form filled
                   
                 
                    $('#save').hide();
        $('#cancel').hide();
        $("#loadergif").show();
          var autoReorder=$('#AutoReorder').attr('checked');
        //enable disabled elements
        $('#IsOtherCost').attr('disabled',false);
        //
		$.post("{$_subdomain}/Job/updatePart", $("#updatePartForm").serialize(), function(data) {
            
		    partsTable.fnReloadAjax(null,null,null,true);
                   
		    $("#PartsUsedCount").text(partsTable.fnGetData().length + 1);
		    $("#save").die();
		    $("#updatePartForm").die();
                   
		    $("#updatePartModalContainer").remove().empty();
		    $("#stockSearchContainer").remove().empty();
                    
                   
                    $.colorbox.close();
                     return false;
                   });
                   }
                        {else}//not need change supplier form
                   
               $('#save').hide();
        $('#cancel').hide();
        $("#loadergif").show();
          var autoReorder=$('#AutoReorder').attr('checked');
        //enable disabled elements
        $('#IsOtherCost').attr('disabled',false);
        //
		$.post("{$_subdomain}/Job/updatePart", $("#updatePartForm").serialize(), function(data) {
            
		    partsTable.fnReloadAjax(null,null,null,true);
                   
		    $("#PartsUsedCount").text(partsTable.fnGetData().length + 1);
		    $("#save").die();
		    $("#updatePartForm").die();
                   
		    $("#updatePartModalContainer").remove().empty();
		    $("#stockSearchContainer").remove().empty();
                    {if $ServiceProviderID==134}//gspn samsung orders
                    if(autoReorder==true || autoReorder=='checked'){
                  
                    $('#gspnOrdering').show();
                     $.colorbox.resize({ 'width':400});
                   
                     partid=$.trim(data);
                        putGSPNOrder(partid);
                    
                    }else{
                    
                    $.colorbox.close();
                     return false;
                    }
                    {else}//not gspn samsung order
                     $.colorbox.close();
                     return false;
                    {/if}
		  
		});
                 {/if}//ChangeSupplieronJobPartOrder end
                }else{
                
                
                ///its enought stock no need to order
                     
                    $('#save').hide();
        $('#cancel').hide();
        $("#loadergif").show();
          var autoReorder=$('#AutoReorder').attr('checked');
        //enable disabled elements
        $('#IsOtherCost').attr('disabled',false);
        //
		$.post("{$_subdomain}/Job/updatePart", $("#updatePartForm").serialize(), function(data) {
            
		    partsTable.fnReloadAjax(null,null,null,true);
                   
		    $("#PartsUsedCount").text(partsTable.fnGetData().length + 1);
		    $("#save").die();
		    $("#updatePartForm").die();
                   
		    $("#updatePartModalContainer").remove().empty();
		    $("#stockSearchContainer").remove().empty();
                    
                   
                    $.colorbox.close();
                     return false;
                   });
                   
	return false;
                }
                }
		});
		return false;
	    }
	});

	$("#updatePartForm").submit();
	
	return false;
	
    };


function useThisItem(t)
{
var q=$(t).parent('td').parent('tr').attr('id');
//console.log($(t).parent('td').parent('tr'));
 $.post("{$_subdomain}/Stock/getStockItemData/id="+q,{ },
		    function(data) {
                    en=jQuery.parseJSON(data);
                  // console.log(en);
                  
                $('#stockSearchContainer').hide();
                $('#updatePartModalContainer').show();
                $.colorbox.resize();
                $('#PartDescription').val(en.Description);
                $('#stockDescHid').val(en.Description);
                $('#PartNo').val(en.PartNumber);
                $('#stockCodeHid').val(en.PartNumber);
                $('#SpPartStockTemplateID').val(q);
                $('#UnitCost').val(en.PurchaseCost);
                $('#SupplierID').val(en.DefaultServiceProviderSupplierID).next(".ui-combobox").children('.ui-autocomplete-input').val($("#SupplierID option:selected").text());
                $('#PartStatusID').val(11).next(".ui-combobox").children('.ui-autocomplete-input').val($("#PartStatusID option:selected").text()).attr('readonly','readonly');
                $('#PartStatusID').next(".ui-combobox").children('.ui-combobox-toggle').hide();
                $('#Quantity').val(1);
                $('#OrderNo').val(en.OrderNo);
                //$('#ReceivedDate').val(en.RecDate);
                $('#instockval').val(en.InStockAvailable);
              //  parseFloat(Math.round(num3 * 100) / 100).toFixed(2);
               
                $('#partTemplateID').val(en.SpPartStockTemplateID);
               checkInstock();
                    });
}


function validateQty(){

if(Math.floor($('#Quantity').val()) != $('#Quantity').val() || !$.isNumeric($('#Quantity').val())){

$('#genError_stockDiv').html("Please enter only whole numbers into the Quantity field").dialog(
{       
      title:"Warning",
      resizable:false,
      modal: true,
    buttons: {
        Ok:{ text:"Ok", "class":"gplus-blue", click:function() {
          $( this ).dialog( "close" );
        }}
      }
    }
);
$('#Quantity').val(1);
}else{
if($('#Quantity').val()<1){

$('#genError_stockDiv').html("Please enter a numeric value greater than zero").dialog(
{       
      title:"Warning",
      resizable:false,
      modal: true,
     buttons: {
        Ok:{  text:"Ok","class":"gplus-blue", click:function() {
          $( this ).dialog( "close" );
        }}
      }
    }
);
$('#Quantity').val(1);
}
}
}

function checkInstock(d){
if($('#IsOtherCost').attr('checked')!=true){ //check if sundry item


{if $stockAvailable}
     {$upd=$data.Quantity|default:'0'}
if($('#instockval').val()*1< $('#Quantity').val()*1-{$upd}){
 
//$('#Quantity').val($('#instockval').val());
if(d!=1){

//if stock >0
if($('#instockval').val()*1==0){
$('#no_stockDiv').dialog(
{       
      title:"Warning",
      resizable:false,
      modal: true,
     buttons: {
        Ok:{  text:"Ok","class":"gplus-blue", click:function() {
          $( this ).dialog( "close" );
        }}
      }
    }
);

 $('#PartStatusID').val(2).next(".ui-combobox").children('.ui-autocomplete-input').val($("#PartStatusID option:selected").text()).attr('readonly','readonly');
 //$('#save').hide();
}else{
$('#no_stockDiv').dialog(
{       
      title:"Warning",
      resizable:false,
      modal: true,
      buttons: {
        Ok:{  text:"Ok","class":"gplus-blue", click:function() {
          $( this ).dialog( "close" );
        }}
      }
    }
);

}
}else{
// if stock <0
//$('#PartStatusID').val(2).next(".ui-combobox").children('.ui-autocomplete-input').val($("#PartStatusID option:selected").text());
 //$('#SupplierID').val("").next(".ui-combobox").children('.ui-autocomplete-input').val($("#SupplierID option:selected").text());
  // $('#OrderNo').val("");
  //   $('#ReceivedDate').val("");
    
  
}
}
 $('#SaleCost').val(($('#Quantity').val()*$('#UnitCost').val()).toFixed(2));
 {else}
$('#instockval').hide();
    {/if}
 }//end sundry item
}

function checkAvailableQuantity(){
     $.post("{$_subdomain}/Stock/getStockItemQty/id="+$('#SpPartStockTemplateID').val(),{ },
		    function(data) {
                    en=jQuery.parseJSON(data);
                                        

                    $('#instockval').val(en);
                    });
}

function manuallyInsert(){
$('#stockSearchContainer').hide();
$('#updatePartModalContainer').show();

}


function setSundryPart(){

if($('#IsOtherCost').attr('checked')==true||$('#IsOtherCost').attr('checked')=="checked"){
$('#stockFields').hide();
$('#Quantity').val(1);
$('#PartStatusID').val(11).next(".ui-combobox").children('.ui-autocomplete-input').val($("#PartStatusID option:selected").text()).attr('readonly','readonly');
}else{
$('#stockFields').show();
if($('#instockval').val()*1< $('#Quantity').val()*1-{$upd}){
$('#PartStatusID').val(2).next(".ui-combobox").children('.ui-autocomplete-input').val($("#PartStatusID option:selected").text()).attr('readonly','readonly');
}
}

}


function putGSPNOrder(partID){
$('#insertedPartID').val(partID);
 $.post("{$_subdomain}/SamsungClient/putStockPartOrderReq", { partID: partID }, function(response) {
en=jQuery.parseJSON(response);

if(en['Response']['ResponseStatus']=='F')
    //check gspn response if status = Fail F
{ 
$('#gspnOrdering').hide();

$('#stockCodeHidSpan').html($('#stockCodeHid').val());
$('#gspnErrorDiv').show();
$('#gspnErrorMsg').html(en['Response']['ResponseDescription'] +" "+en['OrderResult']['ItemList']['Remark']);
$.colorbox.resize({ width:900});
}else{
//if order was S chaning status to 03 ordered
partid=$('#insertedPartID').val();
$.post("{$_subdomain}/Stock/changePartsStatus", { parts: partid,status:3 }, function(response) {
partsTable.fnReloadAjax(null,null,null,true);
$.colorbox.close();
});
}
});

}


function selectGSPNErrorReason(){

val=$('input[name=gspnrespRadio]:checked').val();

if(!val)
{
   
$('#gspnErrorText').css({ 'color':'red'});
}else{
 
switch(val){

case "1": setPartUnavailable(); break;
case "2": $.colorbox.close();break;
case "3": removePartFromJob(); break;
}
}
}

function setPartUnavailable(){
partid=$('#insertedPartID').val();
$.post("{$_subdomain}/Stock/changePartsStatus", { parts: partid,status:6 }, function(response) {
partsTable.fnReloadAjax(null,null,null,true);
$.colorbox.close();
});

}

function removePartFromJob(){
$('#gspnErrorDiv').html('<p>Part Number '+$('#stockCodeHid').val()+' ('+$('#stockDescHid').val() +') cannot be ordered from GSPN and has been removed from the order.</p> <p style="margin:auto;text-align: center;"><a  onclick="$.colorbox.close();" id="gspnOk" class="btnConfirm" style="margin-right:10px; display:inline-block; width:60px;cursor:pointer">Ok</a></p>');

$.colorbox.resize();
partid=$('#insertedPartID').val();
$.post("{$_subdomain}/Job/deletePart", { partID: partid,JobID:{$JobID|default:"0"} }, function(response) {
partsTable.fnReloadAjax(null,null,null,true);
});


}

function updateModelFilter(){
$('#AllModelsFilter').attr("checked",false);
var oTable2 = $('#partsResults').dataTable();
oTable2.fnReloadAjax("{$_subdomain}/StockControl/loadSPPartsList/modelID="+$('#modelFilter').val()+"/chargeType={$jobChargeType}/");
}

function showAllModelParts(){
var oTable2 = $('#partsResults').dataTable();
if($('#AllModelsFilter').attr("checked")){
oTable2.fnReloadAjax("{$_subdomain}/StockControl/loadSPPartsList/chargeType={$jobChargeType}/");
}else{

oTable2.fnReloadAjax("{$_subdomain}/StockControl/loadSPPartsList/modelID="+$('#modelFilter').val()+"/chargeType={$jobChargeType}/");
}
}

function addOrderedParts(){
//adding new supplier and qantity from order form to part add form and submiting form
$('#Quantity').val($('#instockval').val()*1+$('#qtyReq').val()*1);
$('#SupplierID').val($('#ServiceProviderSupplier').val());
$('#IgnoreValidation').val("true");

$('#updatePartForm').submit();
}



</script>
<style>
    .ui-combobox input{
        width:240px!important;
        }
    p input{
        width:240px;
        }
    .ui-dialog .ui-dialog-buttonpane button, .buttonw100px{
        width:100px!important;
        }
</style>   

{$chargTypeLegend="Stock"}
{$chargType=""}
    {if $jobChargeType=="c"}{$chargTypeLegend="Chargeable"}{$chargType="C"}{/if}
    {if $jobChargeType=="w"}{$chargTypeLegend="Warranty"}{$chargType="W"}{/if}
    {if $job.EstimateRequired|default:'No'=="Yes"&&$job.EstimateAccepted=="No"}{$chargTypeLegend="Estimate"}{$chargType="E"}{/if}
<div id="stockSearchContainer" {if $mode!="new"} style="display:none;"{/if}>
   
    <fieldset style="width:900px;">
    <legend align="left">Insert {$chargTypeLegend} Parts</legend>
	<br/>
        <p>
            <label class="fieldLabel" style="font-size:11px">Model:</label>
            <select onchange="updateModelFilter()" id="modelFilter">
                <option>Please Select Model</option>
                {foreach $SPModelList as $d}
                    <option value="{$d.ServiceProviderModelID}" {if $job.ModelID==$d.ModelID}selected=selected{/if} >{$d.ModelNumber}</option>
                {/foreach}
            </select>&nbsp;<input onclick="showAllModelParts()" style="width:20px" type="checkbox" id="AllModelsFilter">&nbsp;<span>All Models</span>
        </p>
         <p>
                  
                    
                     <table id="partsResults"  border="0" cellpadding="0" cellspacing="0" class="browse" >
                        <thead>
			    <tr>
                                <th>ID</th>
                                <th>Stock Code</th>
                                <th>Description</th>
                                <th>Warranty Price</th>
                                <th>Chargeable Price</th>
                                <th>Qty In Stock</th>
                                <th>Add</th>
			    </tr>
                        </thead>
                        <tbody>
                          
                          
                        </tbody>
                    </table>  
                    
                    </p>
                    <!--<button id="partManualyButton" type="button" style="float: right" class="gplus-blue">Insert Part Manually</button>-->
                    <button id="" type="button" onclick="$.colorbox.close();" style="float: right" class="gplus-blue">Cancel</button>
                        
    </fieldset>
</div>
<div id="updatePartModalContainer" {if $mode!="update"} style="display:none;"{/if}>
    
    
    {if !isset($data)} 
        
    
    {/if}
    
    <fieldset style="width:900px">
	<legend align="left">{if $mode=="new"}Insert {$chargTypeLegend} Part{elseif $mode=="update"}Edit {$chargTypeLegend} Part{else}Fault Code Only{/if}</legend>
	<br/>
	<form id="updatePartForm" action="" method="post">
            <input type="hidden" id="SpPartStockTemplateID" value="{$data.SpPartStockTemplateID|default:''}" name="SpPartStockTemplateID" />
            <input type="hidden" id="ChargeType" value="{$data.ChargeType|default:''} {$chargType}" name="ChargeType" />
            <input type="hidden" id="IgnoreValidation" value="" />
<div id="block_1" style="width:800px;">
            
	    <div style="display:block; position:relative; float:left; width:400px;">
	    
		<p>
		    <label class="fieldLabel" for="PartDescription">{$page['Labels']['part_description']|escape:'html'}:</label>
		    <input type="text" id="PartDescription" name="PartDescription" value="{$data.PartDescription|default:''}" />
		</p>

		

		<p>
		    <label class="fieldLabel" for="PartNo">{$page['Labels']['part_no']|escape:'html'}:</label>
		    <input type="text" id="PartNo" name="PartNo" value="{$data.PartNo|default:''}" />
		</p>
                <p>
		    <label class="fieldLabel" for="Quantity">{$page['Labels']['part_qty']|escape:'html'}:</label>
                    <input  onblur="validateQty();checkInstock()" type="text" id="Quantity" name="Quantity" value="{$data.Quantity|default:''}" style="width:50px" />
                  {if $stockAvailable} {if $mode!="update"} <span id="stockFields"><span>In Stock:</span> <input name="instockval" readonly="readonly" id="instockval" style="font-weight:bold;border:none;background:none;width:30px;"></span> {/if}{/if}
		</p>
                <p>
		    <label class="fieldLabel" for="OrderStatus">{$page['Labels']['part_order_status']|escape:'html'}:</label>
		   
                    <select  onchange="a($(this).val())" id="PartStatusID" name="PartStatusID" style="width:182px;">
			    <option value="">Select Order Status</option>
			    {foreach $OrderStatus as $code}
				<option value="{$code.PartStatusID}" {if $code.PartStatusID == $data.PartStatusID}selected="selected"{/if}> {$code.PartStatusName}</option>
			    {/foreach}
		    </select>
		</p>
                
                
               </div>
                
               <div style="display:block; position:relative; float:left; width:361px;">
                     
                <p>
		    <label class="fieldLabel" style="width:182px"  for="IsMajorFault">Primary Fault:</label>
		    <input style="width:20px" type="checkbox" name="IsMajorFault" value="1" {if isset($data) && $data.IsMajorFault == "1"}checked="checked"{/if} {if $partsUsed==0}checked=checked{else} onclick="if(this.checked==true){ if(!confirm('{$page['Errors']['primary_part_set']}')){ this.checked=false};}"{/if} /> 
		  
		</p>

		<p>
		    <label class="fieldLabel" style="width:182px" for="IsOtherCost">Sundry Part:</label>
                    <input {if $mode=='update'}disabled=disabled{/if} onclick="setSundryPart()" style="width:20px" type="checkbox" id="IsOtherCost" name="IsOtherCost" value="Y" {if isset($data) && $data.IsOtherCost == "Y"}checked="checked"{/if} /> 
		  
		</p>

		<p>
		    <label class="fieldLabel" style="width:182px" for="IsAdjustment">Warranty Part:</label>
                    <input style="width:20px" {if $jobChargeType=="w"||$jobChargeType=="c"}disabled=disabled{/if} id="IsAdjustment" onclick="setCompulsory();" type="checkbox" name="IsAdjustment" value="Y" {if isset($data) && $data.IsAdjustment == "Y"}checked="checked"{/if} {if $serviceType==2&&$mode=='new'}checked=checked{/if}/> 
		 
		</p>
		<p>
		    <label class="fieldLabel" style="width:182px" for="IsAdjustment">Auto Reorder Replacement:</label>
                    <input style="width:20px" id="AutoReorder"  type="checkbox" name="AutoReorder" value="Yes" {if isset($AutoReorderStatus) && $AutoReorderStatus == "Yes"}checked="checked"{/if} /> 
		 
		</p>
               </div>    
                
   </div>  
                
  <div id="block_2" style="width:800px;">
      <br>
      <br>
                    <hr>
                     <div style="display:block; position:relative; float:left; ">
                        
		    <span class="fieldLabel" for="UnitCost">{$page['Labels']['part_unit_cost']|escape:'html'}:</span>
                    <input type="text" onblur="$('#SaleCost').val(($('#Quantity').val()*$('#UnitCost').val()).toFixed(2));" id="UnitCost" name="UnitCost" value="{$data.UnitCost|default:''}" />
		

		
		    <span class="fieldLabel" for="SaleCost">{$page['Labels']['part_sale_cost']|escape:'html'}:</span>
		    <input type="text" id="SaleCost" name="SaleCost" value="{$data.SaleCost|default:''}" />
		

		
		    <span class="fieldLabel" for="VATRate">{$page['Labels']['part_vat_rate']|escape:'html'}:</span>
		    <input type="text" id="VATRate" name="VATRate" value="{$data.VATRate|default:'20.00'}" />
		
                     </div>
 </div>
                    
                    
                    
                    
 <div id="block_3" style="width:800px;">
     <div id="spacer3">
         
     
     <br>
                        <hr>
                        </div>
                     <div style="display:block; position:relative; float:left; width:400px;">
                        <p>
                            <label class="fieldLabel" for="SectionCode">{$page['Labels']['part_section_code']|escape:'html'}<sup class="compy" style="display:none">*</sup>:</label>
                    
                    
		 
                    <select id="SectionCodeID" name="SectionCodeID" style="width:182px;">
			    <option value="">Select Condition Code</option>
			    {foreach $SectionCodes as $code}
				<option value="{$code.PartSectionCodeID}" {if $code.PartSectionCodeID == $data.PartSectionCodeID}selected="selected"{/if}>{$code.PartSectionCode} &nbsp; {$code.PartSectionCodeDescription}</option>
			    {/foreach}
		    </select>
                    
		</p>

		<p>
		    <label class="fieldLabel" for="DefectCode">{$page['Labels']['part_defect_code']|escape:'html'}<sup class="compy" style="display:none">*</sup>:</label>
                     <select id="DefectCodeID" name="DefectCodeID" style="width:182px;">
			    <option value="">Select Condition Code</option>
			    {foreach $DefectCodes as $code}
				<option value="{$code.PartDefectCodeID}" {if $code.PartDefectCodeID == $data.PartDefectCodeID}selected="selected"{/if}>{$code.PartDefectCode} &nbsp; {$code.PartDefectCodeDescription}</option>
			    {/foreach}
		    </select>
		  
		</p>

		<p>
		    <label class="fieldLabel" for="RepairCode">{$page['Labels']['part_repair_code']|escape:'html'}<sup class="compy" style="display:none">*</sup>:</label>
		    
                       <select id="RepairCodeID" name="RepairCodeID" style="width:182px;">
			    <option value="">Select Condition Code</option>
			    {foreach $RepairCodes as $code}
				<option value="{$code.PartRepairCodeID}" {if $code.PartRepairCodeID == $data.PartRepairCodeID}selected="selected"{/if}>{$code.PartRepairCode} &nbsp; {$code.PartRepairCodeDescription}</option>
			    {/foreach}
		    </select>
		</p>
                        
                        </div>
                      <div style="display:block; position:relative; float:left; width:361px;">
                          <p>
		    <label class="fieldLabel" for="CircuitReference">{$page['Labels']['part_circuit_reference']|escape:'html'}:</label>
                    <input style="width:200px" type="text" id="CircuitReference" name="CircuitReference" value="{$data.CircuitReference|default:''}" />
		</p>

		<p>
		    <label class="fieldLabel" for="PartSerialNo">{$page['Labels']['part_serial_no']|escape:'html'}:</label>
		    <input style="width:200px" type="text" id="PartSerialNo" name="PartSerialNo" value="{$data.PartSerialNo|default:''}" />
		</p>
                      </div>
</div>
                
                <div id="block_4" style="display:none">
      <br>
      <br>
      <hr>
        <div style="display:block; position:relative; float:left; width:400px;">
                 <p>
		    <label class="fieldLabel" for="SupplierID">{$page['Labels']['part_supplier']|escape:'html'}:</label>
                    <input type="hidden" id="SupplierID" name="SupplierID" value="{$code.SupplierID|default:""}">
<!--                       <select id="SupplierID" name="SupplierID" style="width:182px;">
			    <option value="">Select Supplier</option>
			    {foreach $suppliers as $code}
				<option value="{$code.SupplierID}" {if $code.SupplierID == $data.SupplierID}selected="selected"{/if}>{$code.CompanyName}</option>
			    {/foreach}
		    </select>-->
		</p>

		<p>
		    <label class="fieldLabel" for="OrderNo">Order No:</label>
		    <input type="text" id="OrderNo" name="OrderNo" value="{$data.OrderNo|default:''}" />
		</p>
                <p>
		    <label class="fieldLabel" for="SupplierOrderNo">{$page['Labels']['part_supplier_order_no']|escape:'html'}:</label>
		    <input type="text" id="SupplierOrderNo" name="SupplierOrderNo" value="{$data.SupplierOrderNo|default:''}" />
		</p>
        </div>
          <div style="display:block; position:relative; float:left; width:361px;">
                <p>
		    <label class="fieldLabel" for="OrderDate">{$page['Labels']['part_ordered']|escape:'html'}:</label>
                    {if $mode=="new"}
		    <input style="width:200px" type="text" id="OrderDate" name="OrderDate" value="{$data.OrderDate|default:''}" />
                    {else}
                        {$data.OrderDate|default:''}
                        <input type="hidden" id="OrderDate" name="OrderDate" value="{$data.OrderDate|default:''}" />
                    {/if}
		</p>

		<p>
		    <label class="fieldLabel" for="DueDate">{$page['Labels']['part_expected']|escape:'html'}:</label>
		    <input style="width:200px" type="text" id="DueDate" name="DueDate" value="{$data.DueDate|default:''}" />
		</p>

		<p>
		    <label class="fieldLabel" for="ReceivedDate">{$page['Labels']['part_received']|escape:'html'}:</label>
		    <input style="width:200px" type="text" id="ReceivedDate" name="ReceivedDate" value="{$data.ReceivedDate|default:''}" />
		</p>
          </div>
  </div>
                <div style="display:none">
		

		

		<p>
		    <label class="fieldLabel" for="PartStatus">{$page['Labels']['part_status']|escape:'html'}:</label>
		    <input  type="text" id="PartStatus" name="PartStatus" value="{$data.PartStatus|default:''}" />
		</p>

		
	    
	    </div>
	    
	    
	    <div style="display:none; position:relative; float:left; width:361px;">
	    
		

		

		<p>
		    <label class="fieldLabel" for="InvoiceNo">{$page['Labels']['part_invoice_no']|escape:'html'}:</label>
		    <input type="text" id="InvoiceNo" name="InvoiceNo" value="{$data.InvoiceNo|default:''}" />
		</p>

		

		<p>
		    <label class="fieldLabel" for="SBPartID">{$page['Labels']['part_sb_part_id']|escape:'html'}:</label>
		    <input type="text" id="SBPartID" name="SBPartID" value="{$data.SBPartID|default:''}" />
		</p>
		
		
	    
		

	    </div>
	    
	    <input type="hidden" value="{$data.PartID|default:""}" name="PartID" />
	    <input type="hidden" value="{$JobID|default:""}" name="JobID" />
	    
	</form>
	
	<div style="text-align:center; position:relative; display:block; float:left; width:100%; clear:both; margin:10px 0px;">
            <a  onclick="savePartsStock()" id="save" class="btnConfirm" style="margin-right:10px; display:inline-block; width:60px;cursor:pointer">Save</a>
	    <a href="#" id="cancel" class="btnCancel" style="display:inline-block; float:none; width:60px;">Cancel</a>
            <span id="loadergif" style="display: none" class="blueText" >
		    <img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >
		</span>
	</div>
	
    </fieldset>
    
</div>
                
                <div id="gspnOrdering" style="display:none;width:300px">
                    <input type="hidden" id="insertedPartID">
                    <input type="hidden" id="stockCodeHid">
                    <input type="hidden" id="stockDescHid">
                    <p style="margin:auto;text-align: center">Creating order on Samung GSPN</p>
                    <p style="margin:auto;text-align: center">Please wait</p>
                    
         
                    <p  class="blueText" style="margin:auto;text-align: center">
		    <img src="{$_subdomain}/css/Skins/{$_theme}/images/loader.gif" width="16" height="16" >
		</p>
                </div>
                
                <div id="gspnErrorDiv" style="display:none;width:800px">
                    <fieldset id="gspnErrorFieldset">
                        <legend>GSPN Error Report</legend>
                   
                        <p style="">GSPN has returned the following error for Part Number <span id="stockCodeHidSpan"></span>:</p>
                    <p style="font-weight: bold" id="gspnErrorMsg"></p>
                    <br>
                    <p id="gspnErrorText" style="fonst-size:12px;">Please select one of the options below that best describes the error message:</p>
                    <p><input type="radio" name="gspnrespRadio" value="1" style="width:25px;margin-left:200px;"><span>Item no longer available</span></p>
                    <p><input type="radio" name="gspnrespRadio" value="2" style="width:25px;margin-left:200px;"><span>Item out of stock or on back order</span></p>
                    <p><input type="radio" name="gspnrespRadio" value="3" style="width:25px;margin-left:200px;"><span>Stock code unknown</span></p>
                    <p style="margin:auto;text-align: center;"><a  onclick="selectGSPNErrorReason()" id="gspnOk" class="btnConfirm" style="margin-right:10px; display:inline-block; width:60px;cursor:pointer">Ok</a></p>
                     </fieldset>
                   
                </div>
                <div title="Warning" id="no_stockDiv" style="display:none">
                <p>{$page['Errors']['not_in_stock']|escape:'html'}</p>
                </div>
                <div title="Warning" id="chnaged_stockDiv" style="display:none">
                <p>WARNING: While you have been processing this record stock quantities have changed. If you wish to proceed you must re-enter the quntity required</p>
                </div>
                
                   <div title="Warning" id="genError_stockDiv" style="display:none">
                
                </div>
                
                <div id="orderHolderDiv" style="display: none">
                
                </div>
                
                <div id="modelChnageDiv" style="display: none">
                The model number has been changed. If this is the correct model number, it must now be changed on the job.
                </div>
                <div id="modelChnageDivConf" style="display: none">
                The Job model number has been changed successfully.
                </div>