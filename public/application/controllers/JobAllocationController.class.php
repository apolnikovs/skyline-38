<?php

require_once('CustomSmartyController.class.php');


/**
 * Description
 *
 * This class handles all actions of Job Allocation under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 * 01/07/2013          Thirumalesh Bandi      Added page title and page description dynamically for every page.
 */

class JobAllocationController extends CustomSmartyController {
    
    public $config;  
    public $session;
    public $skyline;
    public $messages;
    public $lang = 'en';
    public $page;
    public $statuses  = array(
                     
                     0=>array('Name'=>'Active', 'Code'=>'Active'),
                     1=>array('Name'=>'In-Active', 'Code'=>'In-active')
                    
                     );
    public $SkylineBrandID=1000;
    
    
    
    
    public function __construct() { 
        
        parent::__construct(); 
        
       /* ==========================================
         * Read Application Config file.
         * ==========================================
        */
        
        $this->config = $this->readConfig('application.ini');
        
       /* ==========================================
         *  Initialise Session Model
         * ==========================================
         */
               
        $this->session = $this->loadModel('Session'); 
        
        /* ==========================================
         *  Initialise Messages Model
         * ==========================================
         */
               
        $this->messages = $this->loadModel('Messages'); 
        
        //$this->smarty->assign('_theme', 'skyline');
        
       if (isset($this->session->UserID)) {

            $user_model = $this->loadModel('Users');
            $this->user = $user_model->GetUser( $this->session->UserID );
            $this->smarty->assign('_theme', $this->user->Skin);
        
	    $this->smarty->assign('loggedin_user', $this->user);

            //AP7005 - Job Allocation Admin Section access permisson.
            if($this->user->SuperAdmin || isset($this->user->Permissions['AP7005']) || $this->user->UserType=="Network")
            {
                $this->smarty->assign('name',$this->user->Name);
                $this->smarty->assign('session_user_id', $this->session->UserID);
            
                if($this->user->BranchName)
                {
                    $this->smarty->assign('logged_in_branch_name', " - ".$this->user->BranchName." ".$this->config['General']['Branch']." ");
                }
                else
                {
                    $this->smarty->assign('logged_in_branch_name', "");
                }

                if($this->session->LastLoggedIn)
                {
                    $this->smarty->assign('last_logged_in', " - ".$this->config['General']['LastLoggedin']." ".date("jS F Y G:i", $this->session->LastLoggedIn));
                }
                else
                {
                    $this->smarty->assign('last_logged_in', '');
                } 

                $topLogoBrandID = $this->user->DefaultBrandID;                
            }
            else
            {
                $this->redirect('SystemAdmin',null, null);
            }
            

        } else {
           $topLogoBrandID = isset($_COOKIE['brand'])?$_COOKIE['brand']:0;
            
            $this->smarty->assign('session_user_id','');
            $this->smarty->assign('name','');
            $this->smarty->assign('last_logged_in','');
            $this->smarty->assign('logged_in_branch_name', ''); 
            $this->smarty->assign('_theme', 'skyline');
            
            $this->redirect('index',null, null);

        }
        
        if($topLogoBrandID)
        {
            $skyline = $this->loadModel('Skyline');
            $topBrandLogo = $skyline->getBrand($topLogoBrandID);

            $this->smarty->assign('_brandLogo', (isset($topBrandLogo[0]['BrandLogo']))?$topBrandLogo[0]['BrandLogo']:'');
        }
        else
        {
            $this->smarty->assign('_brandLogo', '');
        }
        
        $this->smarty->assign('showTopLogoBlankBox', false);
        
         if (isset($this->session->lang)) {
            $this->lang = $this->session->lang;
        }
        
        
        
        
       
    }
       
    
     /**
     * Description
     * 
     * This method is used to process the data i.e fething, updating and inserting.
     * 
     * @param array $args It contains modelname as first element where second element as process type
     * @return void It prints json encoded result where datatable using this data.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
     public function ProcessDataAction($args)
     {
            $modelName = isset($args[0])?$args[0]:'';
            $type      = isset($args[1])?$args[1]:'';

            if($modelName && $modelName!='')
            {
                
                
                $this->page =  $this->messages->getPage(lcfirst($modelName), $this->lang);
                
                
                $model  = $this->loadModel($modelName);
                
                if($type=='fetch')
                {
                    $_POST['firstArg']  = isset($args[2])?$args[2]:'';
                    $_POST['secondArg'] = isset($args[3])?$args[3]:'';
                    $_POST['thirdArg'] = isset($args[4])?$args[4]:'';
                    
                    $result     =   $model->fetch($_POST);
                }
                else if($type=='fetchUnallocatedJobs')
                {
                    $_POST['firstArg']  = isset($args[2])?$args[2]:false;
                    
                    $result     =   $model->fetchUnallocatedJobs($_POST);
                }   
                else if($type=='assignServiceCentre')
                {
                    $_POST['ServiceProviderID']  = isset($args[2])?$args[2]:false;
                    $_POST['JobID']              = isset($args[3])?$args[3]:false;
                    
                    $result     =   $model->assignServiceCentre($_POST);
                } 
                else if($type=='cancelJob')
                {  
                    $result     =   $model->cancelJob($_POST);
                }
                else
                {
                    $result     = $model->processData($_POST);
                }    
                echo json_encode( $result );
           }
           return;
     }
    
     
     
      public function indexAction( /*$args*/ ) { 
          
          
      }
      
       
      /**
     * Description
     * 
     * This method is used for to manuplate data of Central Service Allocations Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
       public function centralServiceAllocationsAction(  $args  ) { 
       
       
            $firstArg          = isset($args[0])?$args[0]:null;
            $secondArg         = isset($args[1])?$args[1]:null;
            $thirdArg          = isset($args[2])?$args[2]:null;
          
            $Skyline              = $this->loadModel('Skyline');
            
            
            //This is for ajax call of get clients for selected network.
            if($firstArg=="getClients")
            {
                $clients = array();
                if($secondArg)
                {
                   $clients =  $Skyline->getNetworkClients($secondArg);
                }
                
                echo json_encode($clients);
            }
            else if($firstArg=="getManufacturers")//This is for ajax call of get manufacturers for selected network.
            {
                $manufacturers = array();
                if($secondArg)
                {
                   $manufacturers =  $Skyline->getManufacturer('', $secondArg);
                }
                
                echo json_encode($manufacturers);
            }
            else if($firstArg=="getServiceTypes")//This is for ajax call of get service types for selected jobtype and client.
            {
                $fourthArg    = isset($args[3])?$args[3]:null;
                
                $serviceTypes = array();
                
                $serviceTypes =  $Skyline->getServiceTypes($this->user->DefaultBrandID, $secondArg, $thirdArg, $fourthArg);
                
                
                echo json_encode($serviceTypes);
            }
            else if($firstArg=="getUnitTypes")//This is for ajax call of get unit types for selected repair skill and client.
            {
                
                $unitTypes = array();
                
                $unitTypes =  $Skyline->getUnitTypes(null, $thirdArg, $secondArg);
                
                
                echo json_encode($unitTypes);
            }
            else if($firstArg=="getCounties")//This is for ajax call of get clients for selected country.
            {
                $counties = array();
                
                $counties =  $Skyline->getCounties($secondArg, $this->user->DefaultBrandID);
                
                
                echo json_encode($counties);
            }
            else if($firstArg=="getServiceProviders")//This is for ajax call of get service provider for selected network.
            {
                $serviceProviders = array();
                
                $serviceProviders =  $Skyline->getNetworkServiceProviders($secondArg);
                
                
                echo json_encode($serviceProviders);
            }
            else
            {    
            
                $networks             = $Skyline->getNetworks();


                $countries            = array();
                $repairSkills         = array();
                $clients              = array();
                $manufacturers        = array();
                $jobTypes             = array();
                $serviceTypes         = array();
                $unitTypes            = array();
                $counties             = array();
                $serviceProviders     = array();



                $this->smarty->assign('statuses', $this->statuses);
                $this->smarty->assign('networks', $networks);





                $this->page =  $this->messages->getPage('centralServiceAllocations', $this->lang);
                
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('centralServiceAllocations');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];

                
                $this->smarty->assign('page', $this->page);


                $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin);//this value should be from user calss
                $this->smarty->assign('userPermissions', $this->user->Permissions);//this value should be from user calss
                $this->smarty->assign('accessDeniedMsg', $this->page['Errors']['access_denied_msg']);

                $accessErrorFlag = false;



                switch( $firstArg ) {

                    case 'insert':

                        
                            

                            $countries = $Skyline->getCountries();
                            $this->smarty->assign('countries', $countries);


                            $repairSkills = $Skyline->getRepairSkills();
                            $this->smarty->assign('repairSkills', $repairSkills);

                            if($secondArg)
                            {
                                $clients = $Skyline->getNetworkClients($secondArg);
                                $manufacturers = $Skyline->getManufacturer('', $secondArg);
                                $serviceProviders = $Skyline->getNetworkServiceProviders($secondArg);
                            }     

                            $this->smarty->assign('clients', $clients);
                            $this->smarty->assign('manufacturers', $manufacturers);


                            $brandJobTypes = $Skyline->getBrandsJobTypes($this->user->DefaultBrandID);



                            $jobTypes      = isset($brandJobTypes[$this->user->DefaultBrandID])?$brandJobTypes[$this->user->DefaultBrandID]:array();


                            $this->smarty->assign('jobTypes', $jobTypes);
                            
                            
                            
                            
                            $this->smarty->assign('serviceProviders', $serviceProviders);
                            


                            $networkName = '';
                            if($secondArg)
                            {
                                $networkName = $Skyline->getNetworkName($secondArg);
                            }

                           //We are fetching data for the selected row where user clicks on copy button
                            $fourthArg    = isset($args[3])?$args[3]:null;
                            $args['CentralServiceAllocationID']       = $fourthArg;
                            if($args['CentralServiceAllocationID']!='')
                            {
                                $model     = $this->loadModel('CentralServiceAllocations'); 
                                $datarow  = $model->fetchRow($args);
                                $datarow['CentralServiceAllocationID'] = '';
                                $datarow['Status'] = 'Active';
                                
                                
                                
                                $serviceTypes =  $Skyline->getServiceTypes($this->user->DefaultBrandID, $datarow['JobTypeID'], $datarow['NetworkID'], $datarow['ClientID']);
                                $unitTypes =  $Skyline->getUnitTypes(null, $datarow['ClientID'], $datarow['RepairSkillID']);
                                $counties =  $Skyline->getCounties($datarow['CountryID'], $this->user->DefaultBrandID);
                            }
                            else
                            {   
                                 $datarow = array(

                                    'CentralServiceAllocationID'=>'', 
                                    'NetworkID'=>$secondArg, 
                                    'NetworkName'=>$networkName, 
                                    'CountryID'=>'', 
                                    'ManufacturerID'=>$thirdArg, 
                                    'RepairSkillID'=>'', 
                                    'JobTypeID'=>'', 
                                    'ServiceTypeID'=>'', 
                                    'UnitTypeID'=>'', 
                                    'CountyID'=>'', 
                                    'ClientID'=>'', 
                                    'ServiceProviderID'=>'', 
                                    'Status'=>'Active'

                                );
                            }
                           
                            $this->smarty->assign('serviceTypes', $serviceTypes);
                            $this->smarty->assign('unitTypes', $unitTypes);
                            $this->smarty->assign('counties', $counties);
                            



                            $this->smarty->assign('datarow', $datarow);
                            $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                            $this->smarty->assign('accessErrorFlag', $accessErrorFlag);


                            $htmlCode = $this->smarty->fetch('centralServiceAllocationsForm.tpl');

                            echo $htmlCode;

                        break;


                    case 'update':

                            $args['CentralServiceAllocationID']  = $secondArg;
                            

                            $model                   = $this->loadModel('CentralServiceAllocations');

                            $datarow  = $model->fetchRow($args);
                            
                            
                            
                            $countries = $Skyline->getCountries();
                            $this->smarty->assign('countries', $countries);


                            $repairSkills = $Skyline->getRepairSkills();
                            $this->smarty->assign('repairSkills', $repairSkills);

                            if($datarow['NetworkID'])
                            {
                                $clients = $Skyline->getNetworkClients($datarow['NetworkID']);
                                $manufacturers = $Skyline->getManufacturer('', $datarow['NetworkID']);
                                $serviceProviders = $Skyline->getNetworkServiceProviders($datarow['NetworkID']);
                            }     

                            $this->smarty->assign('clients', $clients);
                            $this->smarty->assign('manufacturers', $manufacturers);


                            $brandJobTypes = $Skyline->getBrandsJobTypes($this->user->DefaultBrandID);



                            $jobTypes      = isset($brandJobTypes[$this->user->DefaultBrandID])?$brandJobTypes[$this->user->DefaultBrandID]:array();

                            $this->smarty->assign('jobTypes', $jobTypes);
                            
                            
                            
                            $serviceTypes =  $Skyline->getServiceTypes($this->user->DefaultBrandID, $datarow['JobTypeID'], $datarow['NetworkID'], $datarow['ClientID']);
                            $this->smarty->assign('serviceTypes', $serviceTypes);
                            
                            
                            $unitTypes =  $Skyline->getUnitTypes(null, $datarow['ClientID'], $datarow['RepairSkillID']);
                            $this->smarty->assign('unitTypes', $unitTypes);
                            
                            $counties =  $Skyline->getCounties($datarow['CountryID'], $this->user->DefaultBrandID);
                            $this->smarty->assign('counties', $counties);
                            
                            
                            $this->smarty->assign('serviceProviders', $serviceProviders);
                            
                            $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

                            $this->smarty->assign('datarow', $datarow);

                            $this->smarty->assign('accessErrorFlag', $accessErrorFlag);

                            $htmlCode = $this->smarty->fetch('centralServiceAllocationsForm.tpl');

                        echo $htmlCode;

                        break;

                    default:

                        $manufacturers        = array();
                        if($this->user->SuperAdmin && $firstArg)
                        {
                            $manufacturers        = $Skyline->getManufacturer('', $firstArg);
                        }
                        else if($this->user->NetworkID)
                        {
                             $manufacturers        = $Skyline->getManufacturer('', $this->user->NetworkID);
                             $firstArg             = $this->user->NetworkID;
                        }

                        $this->smarty->assign('manufacturers', $manufacturers);

                        $this->smarty->assign('nId', $firstArg);
                        $this->smarty->assign('mId', $secondArg);
                        $this->smarty->display('centralServiceAllocations.tpl'); 

                    }
            }
       } 
       
       
       
       
       
       
       
       
       
        /**
     * Description
     * 
     * This method is used for to manuplate data of Town Allocations Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
       public function townAllocationsAction(  $args  ) { 
       
       
            $firstArg          = isset($args[0])?$args[0]:null;
            $secondArg         = isset($args[1])?$args[1]:null;
            $thirdArg          = isset($args[2])?$args[2]:null;
            $fourthArg         = isset($args[3])?$args[3]:null;
          
         
            $Skyline              = $this->loadModel('Skyline');
            
            
            //This is for ajax call of get clients for selected network.
            if($firstArg=="getClients")
            {
                $clients = array();
                if($secondArg)
                {
                   $clients =  $Skyline->getNetworkClients($secondArg);
                }
                
                echo json_encode($clients);
            }
            else if($firstArg=="getManufacturers")//This is for ajax call of get manufacturers for selected network.
            {
                $manufacturers = array();
                if($secondArg)
                {
                   $manufacturers =  $Skyline->getManufacturer('', $secondArg);
                }
                
                echo json_encode($manufacturers);
            }
            else if($firstArg=="getServiceTypes")//This is for ajax call of get service types for selected jobtype and client.
            {
                
                
                $serviceTypes = array();
                
                $serviceTypes =  $Skyline->getServiceTypes($this->user->DefaultBrandID, $secondArg, $thirdArg, $fourthArg);
                
                
                echo json_encode($serviceTypes);
            }
            else if($firstArg=="getCounties")//This is for ajax call of get clients for selected country.
            {
                $counties = array();
                
                $counties =  $Skyline->getCounties($secondArg, $this->user->DefaultBrandID);
                
                
                echo json_encode($counties);
            }
            else if($firstArg=="getServiceProviders")//This is for ajax call of get service provider for selected network.
            {
                $serviceProviders = array();
                
                $serviceProviders =  $Skyline->getNetworkServiceProviders($secondArg);
                
                
                echo json_encode($serviceProviders);
            }
            else
            {    
            
                $networks             = $Skyline->getNetworks();


                $countries            = array();
                $repairSkills         = array();
                $clients              = array();
                $manufacturers        = array();
                $jobTypes             = array();
                $serviceTypes         = array();
                $counties             = array();
                $serviceProviders     = array();



                $this->smarty->assign('statuses', $this->statuses);
                $this->smarty->assign('networks', $networks);





                $this->page =  $this->messages->getPage('townAllocations', $this->lang);
                
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('townAllocations');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];

                
                $this->smarty->assign('page', $this->page);


                $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin);//this value should be from user calss
                $this->smarty->assign('userPermissions', $this->user->Permissions);//this value should be from user calss
                $this->smarty->assign('accessDeniedMsg', $this->page['Errors']['access_denied_msg']);

                $accessErrorFlag = false;



                switch( $firstArg ) {

                    case 'insert':

                        
                            

                            $countries = $Skyline->getCountries();
                            $this->smarty->assign('countries', $countries);


                            $repairSkills = $Skyline->getRepairSkills();
                            $this->smarty->assign('repairSkills', $repairSkills);

                            if($secondArg)
                            {
                                $clients = $Skyline->getNetworkClients($secondArg);
                                $manufacturers = $Skyline->getManufacturer('', $secondArg);
                                $serviceProviders = $Skyline->getNetworkServiceProviders($secondArg);
                            }     

                            $this->smarty->assign('clients', $clients);
                            $this->smarty->assign('manufacturers', $manufacturers);


                            $brandJobTypes = $Skyline->getBrandsJobTypes($this->user->DefaultBrandID);



                            $jobTypes      = isset($brandJobTypes[$this->user->DefaultBrandID])?$brandJobTypes[$this->user->DefaultBrandID]:array();


                            $this->smarty->assign('jobTypes', $jobTypes);
                            
                            
                            
                            
                            $this->smarty->assign('serviceProviders', $serviceProviders);
                            


                            $networkName = '';
                            if($secondArg)
                            {
                                $networkName = $Skyline->getNetworkName($secondArg);
                            }

                           //We are fetching data for the selected row where user clicks on copy button
                           
                            $args['TownAllocationID']       = isset($args[4])?$args[4]:null;
                            if($args['TownAllocationID']!='')
                            {
                                $model     = $this->loadModel('TownAllocations'); 
                                $datarow  = $model->fetchRow($args);
                                $datarow['TownAllocationID'] = '';
                                $datarow['Status'] = 'Active';
                                
                                
                                
                                $serviceTypes =  $Skyline->getServiceTypes($this->user->DefaultBrandID, $datarow['JobTypeID'], $datarow['NetworkID'], $datarow['ClientID']);
                                
                                $counties =  $Skyline->getCounties($datarow['CountryID'], $this->user->DefaultBrandID);
                            }
                            else
                            {   
                                 $datarow = array(

                                    'TownAllocationID'=>'', 
                                    'NetworkID'=>$secondArg, 
                                    'NetworkName'=>$networkName, 
                                    'CountryID'=>$fourthArg, 
                                    'Town'=>'', 
                                    'ManufacturerID'=>$thirdArg, 
                                    'RepairSkillID'=>'', 
                                    'JobTypeID'=>'', 
                                    'ServiceTypeID'=>'', 
                                    'CountyID'=>'', 
                                    'ClientID'=>'', 
                                    'ServiceProviderID'=>'', 
                                    'Status'=>'Active'

                                );
                            }
                           
                            $this->smarty->assign('serviceTypes', $serviceTypes);
                            $this->smarty->assign('counties', $counties);
                            



                            $this->smarty->assign('datarow', $datarow);
                            $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                            $this->smarty->assign('accessErrorFlag', $accessErrorFlag);


                            $htmlCode = $this->smarty->fetch('townAllocationsForm.tpl');

                            echo $htmlCode;

                        break;


                    case 'update':

                            $args['TownAllocationID']  = $secondArg;
                            

                            $model                   = $this->loadModel('TownAllocations');

                            $datarow  = $model->fetchRow($args);
                            
                            
                            
                            $countries = $Skyline->getCountries();
                            $this->smarty->assign('countries', $countries);


                            $repairSkills = $Skyline->getRepairSkills();
                            $this->smarty->assign('repairSkills', $repairSkills);

                            if($datarow['NetworkID'])
                            {
                                $clients = $Skyline->getNetworkClients($datarow['NetworkID']);
                                $manufacturers = $Skyline->getManufacturer('', $datarow['NetworkID']);
                                $serviceProviders = $Skyline->getNetworkServiceProviders($datarow['NetworkID']);
                            }     

                            $this->smarty->assign('clients', $clients);
                            $this->smarty->assign('manufacturers', $manufacturers);


                            $brandJobTypes = $Skyline->getBrandsJobTypes($this->user->DefaultBrandID);



                            $jobTypes      = isset($brandJobTypes[$this->user->DefaultBrandID])?$brandJobTypes[$this->user->DefaultBrandID]:array();

                            $this->smarty->assign('jobTypes', $jobTypes);
                            
                            
                            
                            $serviceTypes =  $Skyline->getServiceTypes($this->user->DefaultBrandID, $datarow['JobTypeID'], $datarow['NetworkID'], $datarow['ClientID']);
                            $this->smarty->assign('serviceTypes', $serviceTypes);
                            
                            
                            
                            $counties =  $Skyline->getCounties($datarow['CountryID'], $this->user->DefaultBrandID);
                            $this->smarty->assign('counties', $counties);
                            
                            
                            $this->smarty->assign('serviceProviders', $serviceProviders);
                            
                            $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

                            $this->smarty->assign('datarow', $datarow);

                            $this->smarty->assign('accessErrorFlag', $accessErrorFlag);

                            $htmlCode = $this->smarty->fetch('townAllocationsForm.tpl');

                        echo $htmlCode;

                        break;

                    default:

                        $manufacturers        = array();
                        if($this->user->SuperAdmin && $firstArg)
                        {
                            $manufacturers        = $Skyline->getManufacturer('', $firstArg);
                        }
                        else if($this->user->NetworkID)
                        {
                             $manufacturers        = $Skyline->getManufacturer('', $this->user->NetworkID);
                             $firstArg             = $this->user->NetworkID;
                        }

                        $this->smarty->assign('manufacturers', $manufacturers);
                        $countries = $Skyline->getCountries();
                        $this->smarty->assign('countries', $countries);

                        $this->smarty->assign('nId', $firstArg);
                        $this->smarty->assign('mId', $secondArg);
                        $this->smarty->assign('cId', $thirdArg);
                        
                        $this->smarty->display('townAllocations.tpl'); 

                    }
            }
       }
       
       
       
       
       
       
       
       /**
     * Description
     * 
     * This method is used for to manuplate data of Postcode Allocations Page.
     * 
     * @param array $args It contains action type as first element i.e insert/update and second element as primary key of selected row
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
       public function postcodeAllocationsAction(  $args  ) { 
       
            $firstArg          = isset($args[0])?$args[0]:null;
            $secondArg         = isset($args[1])?$args[1]:null;
            $thirdArg          = isset($args[2])?$args[2]:null;
            $fourthArg         = isset($args[3])?$args[3]:null;
         
            $Skyline              = $this->loadModel('Skyline');
            
            if($firstArg=="uploadFiles")//Uploading Files...
            {
                
                /**
                * 
                *
                * Copyright 2009, Moxiecode Systems AB
                * Released under GPL License.
                *
                * License: http://www.plupload.com/license
                * Contributing: http://www.plupload.com/contributing
                */

                // HTTP headers for no cache etc
                header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
                header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
                header("Cache-Control: no-store, no-cache, must-revalidate");
                header("Cache-Control: post-check=0, pre-check=0", false);
                header("Pragma: no-cache");

                // Settings
                //$targetDir = ini_get("upload_tmp_dir") . DIRECTORY_SEPARATOR . "plupload";
               // $targetDir = 'newuploads';
                $targetDir = APPLICATION_PATH."/uploads/postcodeAreas";
                

                $cleanupTargetDir = true; // Remove old files
                $maxFileAge = 5 * 3600; // Temp file age in seconds

                // 5 minutes execution time
                @set_time_limit(5 * 60);

                // Uncomment this one to fake upload time
                // usleep(5000);

                // Get parameters
                $chunk    = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
                $chunks   = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
                $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

                //Appending file name at the begening - This line is added by Nag.
                $prefix = $secondArg;
                if(isset($_FILES['file']['name']))
                {
                    $fext        = strrpos($_FILES['file']['name'], '.');
                    $fileName_1 = substr($_FILES['file']['name'], 0, $fext);
                    $fileName_2 = substr($_FILES['file']['name'], $fext);
                    
                    if($prefix)
                    {
                        $prefix = $prefix."-".$fileName_1;
                    }
                }//Nag's code ends here..
                
                


                // Clean the fileName for security reasons
                $fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

                // Make sure the fileName is unique but only if chunking is disabled
                if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
                        $ext = strrpos($fileName, '.');
                        $fileName_a = substr($fileName, 0, $ext);
                        $fileName_b = substr($fileName, $ext);

                        $count = 1;
                        while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
                                $count++;

                        $fileName = $fileName_a . '_' . $count . $fileName_b;
                }

                //This line is added by Nag.
                if($fileName && $prefix)
                {
                        $fileName = $prefix.'-'.$fileName;
                }

                $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;




                // Create target dir
                if (!file_exists($targetDir))
                        @mkdir($targetDir);

                // Remove old temp files	
                if ($cleanupTargetDir && is_dir($targetDir) && ($dir = opendir($targetDir))) {
                        while (($file = readdir($dir)) !== false) {
                                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                                // Remove temp file if it is older than the max age and is not the current file
                                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
                                        @unlink($tmpfilePath);
                                }
                        }

                        closedir($dir);
                } else
                        die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');


                // Look for the content type header
                if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
                        $contentType = $_SERVER["HTTP_CONTENT_TYPE"];

                if (isset($_SERVER["CONTENT_TYPE"]))
                        $contentType = $_SERVER["CONTENT_TYPE"];

                // Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
                if (strpos($contentType, "multipart") !== false) {
                        if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
                                // Open temp file
                                $out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
                                if ($out) {
                                        // Read binary input stream and append it to temp file
                                        $in = fopen($_FILES['file']['tmp_name'], "rb");

                                        if ($in) {
                                                while ($buff = fread($in, 4096))
                                                        fwrite($out, $buff);
                                        } else
                                                die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                                        fclose($in);
                                        fclose($out);
                                        @unlink($_FILES['file']['tmp_name']);
                                } else
                                        die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
                        } else
                                die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
                } else {
                        // Open temp file
                        $out = fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
                        if ($out) {
                                // Read binary input stream and append it to temp file
                                $in = fopen("php://input", "rb");

                                if ($in) {
                                        while ($buff = fread($in, 4096))
                                                fwrite($out, $buff);
                                } else
                                        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

                                fclose($in);
                                fclose($out);
                        } else
                                die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
                }

                // Check if file has been uploaded
                if (!$chunks || $chunk == $chunks - 1) {
                        // Strip the temp .part suffix off 
                        rename("{$filePath}.part", $filePath);
                }


                // Return JSON-RPC response
                die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');
                
                
            }
            else if($firstArg=="getClients") //This is for ajax call of get clients for selected network.
            {
                $clients = array();
                if($secondArg)
                {
                   $clients =  $Skyline->getNetworkClients($secondArg);
                }
                
                echo json_encode($clients);
            }
            else if($firstArg=="getManufacturers")//This is for ajax call of get manufacturers for selected network.
            {
                $manufacturers = array();
                if($secondArg)
                {
                   $manufacturers =  $Skyline->getManufacturer('', $secondArg);
                }
                
                echo json_encode($manufacturers);
            }
            else if($firstArg=="getServiceTypes")//This is for ajax call of get service types for selected jobtype and client.
            {
                
                
                $serviceTypes = array();
                
                $serviceTypes =  $Skyline->getServiceTypes($this->user->DefaultBrandID, $secondArg, $thirdArg, $fourthArg);
                
                
                echo json_encode($serviceTypes);
            }
            else
            {    
            
                $networks             = $Skyline->getNetworks();


               
                $repairSkills         = array();
                $clients              = array();
                $manufacturers        = array();
                $jobTypes             = array();
                $serviceTypes         = array();



                $this->smarty->assign('statuses', $this->statuses);
                $this->smarty->assign('networks', $networks);


                $this->page =  $this->messages->getPage('postcodeAllocations', $this->lang);
                
                            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('postcodeAllocations');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];

                
                $this->smarty->assign('page', $this->page);


                $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin);//this value should be from user calss
                $this->smarty->assign('userPermissions', $this->user->Permissions);//this value should be from user calss
                $this->smarty->assign('accessDeniedMsg', $this->page['Errors']['access_denied_msg']);

                $accessErrorFlag = false;



                switch( $firstArg ) {

                    case 'insert':

                        

                            $repairSkills = $Skyline->getRepairSkills();
                            $this->smarty->assign('repairSkills', $repairSkills);

                            if($secondArg)
                            {
                                $clients = $Skyline->getNetworkClients($secondArg);
                                $manufacturers = $Skyline->getManufacturer('', $secondArg);
				$this->log("SECOND ARG: " . $secondArg);
                            }     

                            $this->smarty->assign('clients', $clients);
                            $this->smarty->assign('manufacturers', $manufacturers);


                            $brandJobTypes = $Skyline->getBrandsJobTypes($this->user->DefaultBrandID);

                            $jobTypes      = isset($brandJobTypes[$this->user->DefaultBrandID])?$brandJobTypes[$this->user->DefaultBrandID]:array();


                            $this->smarty->assign('jobTypes', $jobTypes);
                            
                            
                            $networkName = '';
                            if($secondArg)
                            {
                                $networkName = $Skyline->getNetworkName($secondArg);
                            }

                           //We are fetching data for the selected row where user clicks on copy button
                           
                               
                            $datarow = array(

                                'PostcodeAllocationID'=>'', 
                                'NetworkID'=>$secondArg, 
                                'NetworkName'=>$networkName, 
                                'ManufacturerID'=>$thirdArg, 
                                'RepairSkillID'=>'', 
                                'JobTypeID'=>'', 
                                'ServiceTypeID'=>'', 
                                'ClientID'=>'', 
                                'OutOfArea'=>'No'

                            );
                            
                            $fileUploadPrefix = rand(100000, 1000000);
                           
                            $this->smarty->assign('serviceTypes', $serviceTypes);
                            $this->smarty->assign('fileUploadPrefix', $fileUploadPrefix);
                           

                            $this->smarty->assign('datarow', $datarow);
                            $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                            $this->smarty->assign('accessErrorFlag', $accessErrorFlag);


                            $htmlCode = $this->smarty->fetch('postcodeAllocationsForm.tpl');

                            echo $htmlCode;

                        break;


                    case 'view':

                            $args['PostcodeAllocationID']  = $secondArg;
                            

                            $model                   = $this->loadModel('PostcodeAllocations');

                            $datarow  = $model->fetchRow($args);
                            
                            
                            $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

                            $this->smarty->assign('datarow', $datarow);

                            $this->smarty->assign('accessErrorFlag', $accessErrorFlag);

                            $htmlCode = $this->smarty->fetch('postcodeAllocationsForm.tpl');

                        echo $htmlCode;

                        break;

                    default:

                        $manufacturers        = array();
                        if($this->user->SuperAdmin && $firstArg)
                        {
                            $manufacturers        = $Skyline->getManufacturer('', $firstArg);
                        }
                        else if($this->user->NetworkID)
                        {
                             $manufacturers        = $Skyline->getManufacturer('', $this->user->NetworkID);
                             $firstArg             = $this->user->NetworkID;
                        }

                        $this->smarty->assign('manufacturers', $manufacturers);
                        
                        $this->smarty->assign('nId', $firstArg);
                        $this->smarty->assign('mId', $secondArg);
                        
                        $this->smarty->display('postcodeAllocations.tpl'); 

                    }
            }
	    
    }
              
       
    
    /**
    Bought Out Guarantee page
    2012-01-24 © Vic <v.rutkunas@pccsuk.com>
    */
    
    public function boughtOutGuaranteeAction() {
	
	$networkModel = $this->loadModel("ServiceNetworks");
	$networks = $networkModel->getAllNetworks();
	$this->smarty->assign("networks", $networks);
	
	$this->page = $this->messages->getPage('boughtOutGuarantee', $this->lang);
        
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('boughtOutGuarantee');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];

        
	$this->smarty->assign('page', $this->page);
	$this->smarty->display('boughtOutGuarantee.tpl'); 
	
    }
       

    
    /**
    Get Edit Bought Out Guarantee modal window
    2012-01-24 © Vic <v.rutkunas@pccsuk.com>
    */
    
    public function getEditBOGAction() {
	
	$bogID = $_POST["bogID"];
	$action = $_POST["action"];

	$guaranteeModel = $this->loadModel("Guarantee");
	
	$this->page = $this->messages->getPage('boughtOutGuarantee', $this->lang);
	$this->smarty->assign('page', $this->page);
	if($action == "edit") {
	    $this->smarty->assign('legend', $this->page["Text"]["edit_form_legend"]);
	    $this->smarty->assign('data', $guaranteeModel->getRecord($bogID));
	} else {
	    $this->smarty->assign('legend', $this->page["Text"]["insert_form_legend"]);
	}
	
	$manModel = $this->loadModel("Manufacturers");
	$this->smarty->assign("manufacturers", $manModel->getAllManufacturers());
	
	$this->smarty->assign("repairSkills", $guaranteeModel->getRepairSkills());
	
	$unitModel = $this->loadModel("UnitTypes");
	$this->smarty->assign("unitTypes", $unitModel->getAllUnitTypes());
	
	$modelsModel = $this->loadModel("Models");
	$this->smarty->assign("models", $modelsModel->getAllModels());
	
        $html = $this->smarty->fetch('boughtOutGuaranteeForm.tpl');
	
	echo($html);
	
    }

    
    
    /**
    Save Edit/Insert Bought Out Guarantee record
    2012-01-29 © Vic <v.rutkunas@pccsuk.com>
    */
    
    public function saveEditBOGAction() {
	
	$guaranteeModel = $this->loadModel("Guarantee");
	$result = $guaranteeModel->saveEditBOG($_POST);
		
    }
    
    
    
    
     /**
     * Description
     * 
     * This method is used for to manuplate data of Unallocated Jobs.
     * 
     * @param array $args 
     * @return void It prints data to the loaded template.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */
       public function unallocatedJobsAction(  $args  ) { 
       
       
            $functionAction    = isset($args[0])?$args[0]:false;
            $selectedRowId     = isset($args[1])?$args[1]:false;
          
            $Skyline              = $this->loadModel('Skyline');
         
            
            
            if($this->user->ClientID || $this->user->BranchID)
            {
                $this->redirect('index',null, null);
            }
            
            if($this->user->SuperAdmin)
            {
                $networks   = $Skyline->getNetworks();
                $nId        = $functionAction;
                $SuperAdmin = true;
            }    
            else
            {
                $nId        = $this->user->NetworkID;
                $SuperAdmin = false;
            }

            
            
            

            $this->page =  $this->messages->getPage('unallocatedJobs', $this->lang);
            
            //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('unallocatedJobs');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];

            
            
            $this->smarty->assign('page', $this->page);


            



                switch( $functionAction ) {

                   

                    case 'allocate':

                            $job_model  = $this->loadModel('Job');

                            $datarow    = $job_model->fetch($selectedRowId);


                            $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

                            $NetworkID = isset($datarow['NetworkID'])?$datarow['NetworkID']:0;
                            
                           
                            
                            
                            $PostCode   = (isset($datarow['ColAddPostcode']) && $datarow['ColAddPostcode'])?trim(substr($datarow['ColAddPostcode'], 0, -3)):false;
                            
                            if(!$PostCode)
                            {    
                                $PostCode   = (isset($datarow['PostalCode']) && $datarow['PostalCode'])?trim(substr($datarow['PostalCode'], 0, -3)):'';
                            }
                            
                            
                            
                            $this->smarty->assign('NetworkID', $NetworkID);
                            $this->smarty->assign('PostCode', $PostCode);
                            $this->smarty->assign('JobBrandName', isset($datarow['Brand'])?$datarow['Brand']:'');
                            
                            $tableRandNumber  = rand(0, 2000);
                            
                            $this->smarty->assign('tableRandNumber', $tableRandNumber);
                            $this->smarty->assign('JobID', $selectedRowId);
                            $this->smarty->assign('cancelJobFormFlag', false);
                            
                            
                            $htmlCode = $this->smarty->fetch('unallocatedJobsForm.tpl');

                        echo $htmlCode;

                        break;
                    
                    case 'cancelJob':


                            $this->smarty->assign('form_legend', $this->page['Text']['cancel_job_legend']);

                            $this->smarty->assign('JobID', $selectedRowId);
                            $this->smarty->assign('cancelJobFormFlag', true);
                            
                            
                            $htmlCode = $this->smarty->fetch('unallocatedJobsForm.tpl');

                            echo $htmlCode;

                            break;
                    
                    default:
                        
                        $this->smarty->assign('nId', $nId);
                        $this->smarty->assign('networks', $networks);
                        $this->smarty->assign('SuperAdmin', $SuperAdmin);
                        $this->smarty->display('unallocatedJobs.tpl'); 

                    }
          
       }
    
    
    
}

?>
