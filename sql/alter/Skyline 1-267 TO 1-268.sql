# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.267');

# ---------------------------------------------------------------------- #
# Add Table sb_authorisation_statuses                                          #
# ---------------------------------------------------------------------- # 
CREATE TABLE sb_authorisation_statuses ( 
											SBAuthorisationStatusID INT(11) NOT NULL AUTO_INCREMENT, 
											SBAuthorisationStatus VARCHAR(100) NOT NULL, 
											ModifiedUserID INT NULL DEFAULT NULL, 
											ModifiedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, 
											PRIMARY KEY (SBAuthorisationStatusID), 
											CONSTRAINT user_TO_sb_authorisation_statuses FOREIGN KEY (ModifiedUserID) REFERENCES user (UserID)
										) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

REPLACE INTO `sb_authorisation_statuses` (`SBAuthorisationStatusID`, `SBAuthorisationStatus`, `ModifiedUserID`, `ModifiedDate`) VALUES
	(1, '16 Authority Required', NULL, '2013-06-05 16:34:45'),
	(2, '16 Authority Query', NULL, '2013-06-05 16:35:29'),
	(3, '17 Authority Issued', NULL, '2013-06-05 16:35:36'),
	(4, '17 Authority Rejected', NULL, '2013-06-05 16:35:42');
	
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.268');
