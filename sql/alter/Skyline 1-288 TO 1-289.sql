# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.288');

# ---------------------------------------------------------------------- #
# Modify job table       												 #
# ---------------------------------------------------------------------- # 
alter table `job`
ADD COLUMN `EstimateRequired` ENUM('Yes','No') NOT NULL DEFAULT 'No' AFTER `InboundFaultCodeID`,
ADD COLUMN `EstimateAccepted` ENUM('Yes','No') NOT NULL DEFAULT 'No' AFTER `EstimateRequired`,
ADD COLUMN `EstimateRejected` ENUM('Yes','No') NOT NULL DEFAULT 'No' AFTER `EstimateAccepted`;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.289');
